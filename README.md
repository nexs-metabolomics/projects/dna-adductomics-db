# The DNA adduct database

The following files are available:

* [The DNA adduct database in Excel format](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/DNA_adduct_database.zip)
* [The DNA adduct database in Word format](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/DNA%20adduct%20database.docx)
* [The DNA adduct database (online)](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/index.html)
* [The compound database in SDF format](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/SDF/DB.sdf)
* [The database of experimental fragments (online)](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/experimental.html)
* [The database of predicted fragments (online)](https://nexs-metabolomics.gitlab.io/projects/dna_adductomics_database/predicted.html)
* [The whole collection of the Excel file, the online databases, the CFM-ID spectra, the Chemdraw files, the mol files and the SDF files of the DNA adducts](https://gitlab.com/nexs-metabolomics/projects/dna_adductomics_database/-/jobs/artifacts/main/download?job=pages)


For contributing to the DNA adductomics database please contact Giorgia La Barbera at [glb@nexs.ku.dk](mailto:glb@nexs.ku.dk).


