#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=[nH]1c(nc2c(c1=O)[n+](cn2[C@@H]1O[C@H](CO)[C@@H](C1)O)CCO)N
#InChiKey=RWAKPFDJWWXAGG-BWZBUEFSSA-O
energy0
152.05669 19.60 8 (6.9359)
196.08290 54.25 9 17 30 38 22 35 25 26 33 89 (19.139 0.014528 0.013376 0.011306 0.0094066 0.0035729 0.0015153 0.0014891 0.00067419 0.00036572)
198.09855 69.80 39 52 51 44 59 57 48 49 56 50 88 (24.005 0.13478 0.13261 0.12709 0.12214 0.056337 0.055488 0.04056 0.01454 0.0084591 1.812e-05)
312.13025 100.00 0 138 128 153 136 155 133 131 132 135 137 134 151 (31.705 2.6035 0.46032 0.41981 0.071051 0.043501 0.023354 0.020882 0.01594 0.0095462 0.0080464 0.0017503 0.00049065)
energy1
152.05669 100.00 8 (36.305)
196.08290 67.48 9 30 17 22 35 26 38 25 89 33 (22.18 0.62673 0.47484 0.46591 0.42212 0.17541 0.089733 0.03825 0.019763 0.0068613)
198.09855 53.24 39 52 44 57 59 48 49 51 50 56 88 (15.334 1.7844 0.73973 0.54306 0.25624 0.187 0.16338 0.15346 0.12709 0.039992 4.4083e-05)
energy2
152.05669 100.00 8 (34.129)
180.08799 16.69 42 40 147 (5.2892 0.26623 0.14045)
196.08290 16.77 9 89 38 35 33 30 26 22 17 25 (2.9501 0.9229 0.73551 0.3628 0.19482 0.18638 0.14578 0.09625 0.090812 0.037062)
198.09855 19.53 39 59 88 57 56 50 44 52 49 48 51 (5.7987 0.26489 0.21408 0.095383 0.054318 0.054025 0.045503 0.044896 0.043784 0.033374 0.017632)
252.10912 12.16 94 71 67 115 69 (2.2209 0.77695 0.73221 0.38417 0.034217)
268.10403 72.21 85 78 73 81 83 77 113 76 103 156 (12.263 2.6802 2.5856 2.5398 1.8208 1.0987 0.72437 0.55198 0.36666 0.011932)

0 312.1302450961 NC1=NC2=C(C(=O)N1)[N+](CCO)=CN2C1CC(O)C(CO)O1
1 119.0702706321 OC1CCOC1C[OH2+]
2 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
3 117.0546205681 OC1CCOC1=C[OH2+]
4 99.0440558841 C=C1OCC=C1[OH2+]
5 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
6 117.0546205681 O=CCC(O)C=C[OH2+] Intermediate Fragment 
7 194.0672509161 [NH3+]C1=NC(O)=C2C(=N1)N=CN2C=CO
8 152.0566862321 [NH3+]C1=NC(=O)C2=NCNC2=N1
9 196.0829009801 [NH3+]C1=NC(O)=C2C(=N1)N=CN2CCO
10 150.0410361681 [NH3+]C1=NC(=O)C2=NC=NC2=N1
11 154.0723362961 [NH3+]C1=NC(=O)C2NCNC2=N1
12 164.0566862321 [CH2+]N1C=NC2=NC(N)=NC(O)=C21
13 166.0723362961 CN1C=NC2=NC([NH3+])=NC(O)=C21
14 168.0879863601 CN1CNC2=NC([NH3+])=NC(O)=C21
15 178.0723362961 C=CN1C=NC2=NC([NH3+])=NC(O)=C21
16 179.0563518841 OC=CN1C=NC2=NC=NC([OH2+])=C21
17 196.0829009801 N=C([NH3+])N=C1N=CN(CCO)C1=C=O Intermediate Fragment 
18 179.0563518841 NC=[NH+]C1=C(C=O)N(C#CO)C=N1
19 154.0611029161 [NH2+]=C1N=CN(CCO)C1=C=O
20 137.0345538201 OC#CN1C=NC=C1C=[OH+]
21 168.0879863601 NC(=[NH2+])N=C1CN(C=CO)C=N1
22 196.0829009801 [NH3+]C(=NC=O)N=C1CN(C=CO)C=N1 Intermediate Fragment 
23 153.0770873281 NC=[NH+]C1=CN(C=CO)C=N1
24 126.0661882961 [NH2+]=C1CN(C=CO)C=N1
25 196.0829009801 NC([NH3+])=NC(=O)C1=CN=CN1C=CO Intermediate Fragment 
26 196.0829009801 N=C1N=CN(C=CO)C1=C(O)N=C[NH3+] Intermediate Fragment 
27 169.0720019481 N=C1N=CN(C=CO)C1=C([NH3+])O
28 128.0818383601 [NH2+]=C1CN(CCO)C=N1
29 152.0454528521 [NH2+]=C1N=CN(C=CO)C1=C=O
30 196.0829009801 [NH3+]C1=NC(O)=CC(N=C=NCCO)=N1 Intermediate Fragment 
31 139.0614372641 C=[NH+]C1=NC(N)=NC(O)=C1
32 137.0457872001 C#[N+]C1=NC(N)=NC(O)=C1
33 196.0829009801 N=CN(C=CO)C1=C(O)N=C([NH3+])N=C1 Intermediate Fragment 
34 169.0720019481 NC1=NC(O)=C([NH+]=CCO)C=N1
35 196.0829009801 CN(C#CO)C1=C(O)N=C([NH3+])N=C1N Intermediate Fragment 
36 127.0614372641 NC1=NC(O)=CC([NH3+])=N1
37 129.0770873281 NC1=NC(O)CC([NH3+])=N1
38 196.0829009801 C=NC1=NC([NH3+])=NC(O)=C1N=CCO Intermediate Fragment 
39 198.0985510441 [NH3+]C1=NC(O)=C2C(=N1)NCN2CCO
40 180.0879863601 [NH3+]C1=NC=C2C(=N1)N=CN2CCO
41 156.0879863601 [NH3+]C1=NC(O)C2NCNC2=N1
42 180.0879863601 CCN1C=NC2=NC([NH3+])=NC(O)=C21
43 181.0720019481 OCCN1C=NC2=NC=NC([OH2+])=C21
44 198.0985510441 N=C([NH3+])N=C1NCN(CCO)C1=C=O Intermediate Fragment 
45 181.0720019481 NC=[NH+]C1=C(C=O)N(C=CO)C=N1
46 139.0502038841 OC=CN1C=NC=C1C=[OH+]
47 170.1036364241 NC(=[NH2+])N=C1CN(CCO)C=N1
48 198.0985510441 [NH3+]C(=NC=O)N=C1CN(CCO)C=N1 Intermediate Fragment 
49 198.0985510441 NC([NH3+])=NC(=O)C1=CN=CN1CCO Intermediate Fragment 
50 198.0985510441 N=C1N=CN(CCO)C1=C(O)N=C[NH3+] Intermediate Fragment 
51 198.0985510441 NC(=O)C1=C(N=C[NH3+])N=CN1CCO Intermediate Fragment 
52 198.0985510441 [NH3+]C1=NC(O)=CC(NC=NCCO)=N1 Intermediate Fragment 
53 141.0770873281 C[NH2+]C1=NC(N)=NC(O)=C1
54 112.0505382321 [NH3+]C1=NC(O)=CC=N1
55 87.0552892641 N=C=[NH+]CCO
56 198.0985510441 N=CN(CCO)C1=C(O)N=C([NH3+])N=C1 Intermediate Fragment 
57 198.0985510441 CN(C=CO)C1=C(O)N=C([NH3+])N=C1N Intermediate Fragment 
58 181.0720019481 C[NH+](C#CO)C1=C(O)N=C(N)N=C1
59 198.0985510441 C=NC1=NC([NH3+])=NC(O)=C1NCCO Intermediate Fragment 
60 200.1142011081 [NH3+]C1=NC(O)C2C(=N1)NCN2CCO
61 202.1298511721 [NH3+]C1=NC(O)C2C(NCN2CCO)N1
62 280.1040303481 [NH3+]C1=NC(O)=C2C(=N1)N(C1CC(O)=CO1)CN2C=CO
63 282.1196804121 [NH3+]C1=NC(O)=C2C(=N1)N(C1CC(O)=CO1)CN2CCO
64 284.1353304761 [NH3+]C1=NC(O)=C2C(=N1)N(C1CC(O)CO1)CN2CCO
65 294.1196804121 C=C1OC(N2CN(CCO)C3=C(O)N=C([NH3+])N=C32)C=C1O
66 276.1091157281 C=C1C=CC(N2CN(C=CO)C3=C(O)N=C([NH3+])N=C32)O1
67 252.1091157281 CC1OC(N2C=NC3=C(O)N=C([NH3+])N=C32)CC1O
68 294.1196804121 C#COC(C=CO)N1CN(CCO)C2=C(O)N=C([NH3+])N=C21 Intermediate Fragment 
69 252.1091157281 [NH3+]C1=NC(O)=C2C(=N1)N(CC#CO)CN2CCO
70 294.1196804121 [NH3+]C1=NC(O)=C2C(=N1)N(C1CCC(=C=O)O1)CN2CCO
71 252.1091157281 [NH3+]C1=NC(O)=C2N=CN(C3CCC(CO)O3)C2=N1
72 266.0883802841 [NH3+]C1=NC(O)=C2N=CN(C3CC(O)C(=CO)O3)C2=N1
73 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(C3CC(O)C(CO)O3)C2=N1
74 250.0934656641 C=C1OC(N2C=NC3=C(O)N=C([NH3+])N=C32)CC1O
75 250.0934656641 [NH3+]C1=NC(O)=C2N=CN(C3CCC(=CO)O3)C2=N1
76 268.1040303481 N=C([NH3+])N=C1C(=C=O)N=CN1C1CC(O)C(CO)O1 Intermediate Fragment 
77 268.1040303481 N=CN(C1=NC([NH3+])=NC(O)=C1)C1CC(O)C(=CO)O1 Intermediate Fragment 
78 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(CCC(O)C(=O)CO)C2=N1 Intermediate Fragment 
79 250.0934656641 [NH3+]C1=NC(O)=C2N=CN(CCC(O)C#CO)C2=N1
80 208.0829009801 [NH3+]C1=NC(O)=C2N=CN(CC=CO)C2=N1
81 268.1040303481 CC(OC(=CO)CO)N1C=NC2=C(O)N=C([NH3+])N=C21 Intermediate Fragment 
82 194.0672509161 CC(=O)N1C=NC2=C(O)N=C([NH3+])N=C21
83 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(C(CCO)OC=CO)C2=N1 Intermediate Fragment 
84 226.0934656641 [NH3+]C1=NC(O)=C2N=CN(C(O)CCO)C2=N1
85 268.1040303481 [NH3+]C1=NC(O)=C2N=CN(C(=O)CC(O)CCO)C2=N1 Intermediate Fragment 
86 224.0778156001 [NH3+]C1=NC(O)=C2N=CN(C(=O)CCO)C2=N1
87 222.0621655361 [NH3+]C1=NC(O)=C2N=CN(C(=O)C=CO)C2=N1
88 198.0985510441 CC(O)N1CNC2=C(O)N=C([NH3+])N=C21
89 196.0829009801 CC(O)N1C=NC2=C(O)N=C([NH3+])N=C21
90 73.0284058201 OCC#C[OH2+]
91 75.0440558841 OCC=C[OH2+]
92 266.0883802841 [NH3+]C1=NC(O)=C2N=CN(C(=O)CC(O)C=CO)C2=N1
93 270.1196804121 [NH3+]C1=NC(O)=C2NCN(C3CC(O)C(CO)O3)C2=N1
94 252.1091157281 [NH3+]C1=NC=C2N=CN(C3CC(O)C(CO)O3)C2=N1
95 158.1036364241 [NH3+]C1=NC(O)C2NCNC2N1
96 160.1192864881 [NH3+]C1NC(O)C2NCNC2N1
97 238.0934656641 [NH3+]C1=NC(O)=C2N=CN(C3CC(O)CO3)C2=N1
98 253.0931313161 OCC1OC(N2C=NC3=C([OH2+])N=CN=C32)CC1O
99 270.1196804121 N=C([NH3+])N=C1C(=C=O)NCN1C1CC(O)C(CO)O1 Intermediate Fragment 
100 253.0931313161 NC=[NH+]C1=C(C=O)N=CN1C1CC(O)C(=CO)O1
101 270.1196804121 [NH3+]C(=NC=O)N=C1C=NCN1C1CC(O)C(CO)O1 Intermediate Fragment 
102 225.0982166961 NC=[NH+]C1=CN=CN1C1CC(O)C(=CO)O1
103 268.1040303481 NC(N=C1C=NCN1C1CC(O)C(=CO)O1)=[NH+]C=O
104 270.1196804121 NC([NH3+])=NC(=O)C1=CN(C2CC(O)C(CO)O2)C=N1 Intermediate Fragment 
105 253.0931313161 NC=[NH+]C(=O)C1=CN(C2CC(O)C(=CO)O2)C=N1
106 211.0713332521 OC=C1OC(N2C=NC(C=[OH+])=C2)CC1O
107 270.1196804121 N=C1C(=C(O)N=C[NH3+])N=CN1C1CC(O)C(CO)O1 Intermediate Fragment 
108 270.1196804121 N=CN(C1=NC([NH3+])=NC(O)=C1)C1CC(O)C(CO)O1 Intermediate Fragment 
109 253.0931313161 C[NH+](C1=NC(N)=NC(O)=C1)C1CC(O)C(=C=O)O1
110 159.0764186321 [NH+]#CNC1CC(O)C(CO)O1
111 270.1196804121 [NH3+]C1=NC(O)=C(N=CNC2CC(O)C(CO)O2)C=N1 Intermediate Fragment 
112 270.1196804121 C=NC1=C(O)N=C([NH3+])N=C1NC1CC(O)C(CO)O1 Intermediate Fragment 
113 268.1040303481 C=[NH+]C1=C(O)N=C(N)N=C1N=C1CC(O)C(CO)O1
114 270.1196804121 [NH3+]C1=NC(O)=C2N=CN(CCC(O)C(O)CO)C2=N1 Intermediate Fragment 
115 252.1091157281 [NH3+]C1=NC(O)=C2N=CN(CCC(O)C=CO)C2=N1
116 270.1196804121 CC(OC(CO)CO)N1C=NC2=C(O)N=C([NH3+])N=C21 Intermediate Fragment 
117 270.1196804121 [NH3+]C1=NC(O)=C2N=CN(C(CCO)OCCO)C2=N1 Intermediate Fragment 
118 270.1196804121 [NH3+]C1=NC(O)=C2N=CN(C(O)CC(O)CCO)C2=N1 Intermediate Fragment 
119 280.1040303481 CN1CN(C2CC(O)C(=C=O)O2)C2=NC([NH3+])=NC(O)=C21
120 282.1196804121 CN1CN(C2CC(O)C(=CO)O2)C2=NC([NH3+])=NC(O)=C21
121 282.1196804121 CN1CN(CCC(O)C(=O)C=O)C2=NC([NH3+])=NC(O)=C21 Intermediate Fragment 
122 282.1196804121 CN1CN(C(=O)CC(O)C=CO)C2=NC([NH3+])=NC(O)=C21 Intermediate Fragment 
123 284.1353304761 CN1CN(C2CC(O)C(CO)O2)C2=NC([NH3+])=NC(O)=C21
124 284.1353304761 CN1CN(C(=O)CC(O)CCO)C2=NC([NH3+])=NC(O)=C21 Intermediate Fragment 
125 294.1196804121 CCN1CN(C2CC(O)C(=C=O)O2)C2=NC([NH3+])=NC(O)=C21
126 182.1036364241 CCN1CNC2=NC([NH3+])=NC(O)=C21
127 295.1036960001 O=C=C1OC(N2CN(CCO)C3=C([OH2+])N=CN=C32)CC1O
128 312.1302450961 N=C([NH3+])N=C1C(=C=O)N(CCO)CN1C1CC(O)C(=CO)O1 Intermediate Fragment 
129 270.1084470321 [NH2+]=C1C(=C=O)N(CCO)CN1C1CC(O)C(=CO)O1
130 253.0818979361 O=C=C1OC(N2C=C(C=[OH+])N(CCO)C2)C=C1O
131 312.1302450961 [NH3+]C(=NC=O)N=C1CN(CCO)CN1C1CC(O)C(=C=O)O1 Intermediate Fragment 
132 312.1302450961 NC([NH3+])=NC(=O)C1=CN(C2CC(O)C(=C=O)O2)CN1CCO Intermediate Fragment 
133 312.1302450961 N=C1C(=C(O)N=C[NH3+])N(CCO)CN1C1CC(O)C(=C=O)O1 Intermediate Fragment 
134 312.1302450961 [NH3+]C1=NC(O)=CC(N(C=NCCO)C2CC(O)C(=CO)O2)=N1 Intermediate Fragment 
135 312.1302450961 [NH3+]C1=NC(O)=C(N(C=NC2CC(O)C(=CO)O2)CCO)C=N1 Intermediate Fragment 
136 312.1302450961 CN(CCO)C1=C(O)N=C([NH3+])N=C1N=C1CC(O)C(=CO)O1 Intermediate Fragment 
137 312.1302450961 CN(C1=NC([NH3+])=NC(O)=C1NCCO)C1CC(O)C(=C=O)O1 Intermediate Fragment 
138 312.1302450961 [NH3+]C1=NC(O)=C2C(=N1)N(CCC(O)C(=O)C=O)CN2CCO Intermediate Fragment 
139 294.1196804121 [NH3+]C1=NC(O)=C2C(=N1)N(CC=C(O)C#CO)CN2CCO
140 276.1091157281 C#CC(O)=CCN1CN(C=CO)C2=C(O)N=C([NH3+])N=C21
141 276.1091157281 [NH3+]C1=NC(O)=C2C(=N1)N(CC#CC#CO)CN2CCO
142 256.1404158561 [NH3+]C1=NC(O)=C2C(=N1)N(CCCO)CN2CCO
143 254.1247657921 [NH3+]C1=NC(O)=C2C(=N1)N(CC=CO)CN2CCO
144 250.0934656641 [NH3+]C1=NC(O)=C2C(=N1)N(CC#CO)CN2C=CO
145 226.1298511721 CCN1CN(CCO)C2=C(O)N=C([NH3+])N=C21
146 224.1142011081 CCN1CN(C=CO)C2=C(O)N=C([NH3+])N=C21
147 180.0879863601 CCN1C=NC2=C(O)N=C([NH3+])N=C21
148 222.0985510441 CCN1CN(C#CO)C2=C(O)N=C([NH3+])N=C21
149 220.0829009801 C=CN1CN(C#CO)C2=C(O)N=C([NH3+])N=C21
150 178.0723362961 C=CN1C=NC2=C(O)N=C([NH3+])N=C21
151 312.1302450961 CC(OC(=C=O)CO)N1CN(CCO)C2=C(O)N=C([NH3+])N=C21 Intermediate Fragment 
152 240.1091157281 CC(=O)N1CN(CCO)C2=C(O)N=C([NH3+])N=C21
153 312.1302450961 [NH3+]C1=NC(O)=C2C(=N1)N(C(CCO)OC#CO)CN2CCO Intermediate Fragment 
154 270.1196804121 [NH3+]C1=NC(O)=C2C(=N1)N(C(=O)CCO)CN2CCO
155 312.1302450961 [NH3+]C1=NC(O)=C2C(=N1)N(C(=O)CC(O)C=CO)CN2CCO Intermediate Fragment 
156 268.1040303481 [NH3+]C1=NC(O)=C2C(=N1)N(C(=O)C=CO)CN2CCO
157 266.0883802841 [NH3+]C1=NC(O)=C2C(=N1)N(C(=O)C#CO)CN2CCO
158 57.0334912001 C#CC[OH2+]
159 226.0934656641 [NH3+]C1=NC(O)=C2C(=N1)N(C=O)CN2CCO
160 224.0778156001 [NH3+]C1=NC(O)=C2C(=N1)N(C=O)CN2C=CO
