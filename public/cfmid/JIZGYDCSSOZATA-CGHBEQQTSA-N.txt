#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=n1cnc2c(c1NC1c3c(C(C(C1O)O)O)ccc1c3c(c(c3c1cccc3)C)C)ncn2[C@@H]1O[C@H](CO)[C@@H](C1)O
#InChiKey=JIZGYDCSSOZATA-CGHBEQQTSA-N
energy0
424.17680 85.79 35 26 23 (17.784 4.5226 0.58964)
442.18737 83.22 59 97 88 117 91 109 123 140 121 122 151 107 73 115 68 143 113 138 86 83 108 131 132 146 112 114 129 (21.594 0.19986 0.082365 0.063596 0.061032 0.032649 0.031817 0.024044 0.020979 0.017193 0.014387 0.0093065 0.009083 0.0078602 0.0075524 0.0066514 0.0056968 0.0050306 0.0029665 0.0025034 0.0022597 0.0021922 0.0021739 0.0013945 0.00097547 0.00054077 0.00042544)
540.22415 64.86 30 25 162 42 22 46 161 29 224 (9.2144 3.4959 2.0334 1.058 0.63028 0.42686 0.2405 0.17673 0.03371)
558.23471 100.00 0 239 177 223 170 191 237 196 203 186 201 169 221 194 163 202 166 218 190 219 212 211 220 234 167 210 (25.249 0.87839 0.21441 0.148 0.042369 0.032257 0.027772 0.026462 0.015847 0.0095929 0.0080133 0.0063804 0.0059676 0.0050634 0.0046616 0.0041571 0.0019922 0.0013946 0.0010639 0.00090869 0.0008376 0.00080362 0.00053442 0.00042339 0.00032097 0.00012366)
energy1
136.06177 6.01 12 (3.7553)
424.17680 100.00 35 23 26 (57.255 3.5041 1.7338)
442.18737 22.34 59 117 97 122 108 86 143 88 109 146 107 73 113 91 151 115 123 140 68 138 83 132 121 129 131 114 112 (12.717 0.29596 0.20118 0.11494 0.096677 0.063909 0.062266 0.055932 0.044373 0.042761 0.037556 0.034714 0.03291 0.030422 0.027843 0.022207 0.017487 0.01484 0.013096 0.012098 0.008048 0.0046896 0.0040587 0.0028137 0.0026232 0.00092551 0.00057267)
energy2
75.04406 6.30 19 (0.9213)
134.04612 34.71 10 (5.0756)
136.06177 100.00 12 (14.622)
234.09855 7.18 16 18 15 (0.4562 0.43994 0.15433)
235.11174 6.31 92 (0.92193)
291.13796 16.25 7 6 (1.243 1.1337)
307.13287 7.76 8 (1.134)
309.14852 8.38 5 (1.2254)
334.14377 4.32 80 (0.63129)
336.15942 3.70 81 (0.54071)
352.15567 6.51 111 (0.95207)
380.15059 5.68 101 96 (0.63538 0.19481)
382.16624 44.99 102 95 (4.4398 2.1387)
398.12477 4.16 128 (0.60798)
411.14517 3.62 147 (0.52931)
412.17680 18.57 105 94 (1.7091 1.006)
413.16082 6.95 148 87 89 (0.69633 0.22487 0.095671)
415.17647 20.96 149 84 144 70 (1.6518 0.51765 0.48644 0.40843)
424.17680 80.98 35 26 23 (7.5549 3.8209 0.46428)
425.16082 21.01 145 69 85 152 (1.0728 0.74934 0.72057 0.5286)
426.15607 14.29 67 66 124 (1.3763 0.43613 0.27707)
426.19245 7.76 36 27 24 (0.5543 0.51378 0.066229)
440.17172 14.51 90 82 58 150 (1.4421 0.37063 0.17408 0.1343)
442.18737 84.78 59 146 73 97 88 91 109 132 107 86 151 83 117 123 68 108 115 129 121 143 140 138 131 112 122 114 113 (8.6812 0.70096 0.4336 0.40631 0.32545 0.19167 0.18687 0.14589 0.14339 0.1277 0.119 0.10188 0.10113 0.093934 0.093016 0.083362 0.07908 0.05577 0.053637 0.053383 0.052865 0.042458 0.034896 0.023437 0.023266 0.022833 0.01898)
468.20302 6.19 232 179 (0.69033 0.2148)
500.22923 4.91 174 181 227 (0.58701 0.081617 0.049283)
530.23980 3.75 172 160 183 (0.34993 0.16322 0.035633)
541.20816 4.08 164 168 (0.36892 0.22777)

0 558.2347101641 Cc1c(C)c2c3c(ccc2c2ccccc12)C(O)C(O)C(O)C3[NH2+]c1ncnc2c1ncn2C1CC(O)C(CO)O1
1 237.0982166961 OC1CC(N2C=NC3=CN=CN=C32)OC1C[OH2+]
2 315.1954711441 CC1C(C)C2C3=CC(=[OH+])C(=O)C(O)=C3CCC2C2CCCCC12
3 313.1798210801 CC1C(C)C2C3=CC(=[OH+])C(=O)C(O)=C3C=CC2C2CCCCC12
4 311.1641710161 CC1C2=C(C=CC3=C(O)C(=O)C(=[OH+])C=C32)C2CCCCC2C1C
5 309.1485209521 CC1=C(C)C2CCCCC2C2=C1C1=CC(=[OH+])C(=O)C(O)=C1C=C2
6 291.1379562681 CC1=C(C)C2=C(C=CC3=C(O)C([OH2+])=CC=C32)C2=C1CCC=C2
7 291.1379562681 CC1=C(C)C2=C(C=CC3=CC(O)=C([OH2+])C=C32)C2=C1CCC=C2
8 307.1328708881 CC1=C(C)C2=C(C=CC3=C(O)C(=O)C(=[OH+])C=C32)C2=C1CCCC2
9 244.0465154721 [NH3+]C1=C2N=CN(C3=CC(=O)C(=C=O)O3)C2=NC=N1
10 134.0461215481 [NH2+]=C1N=CN=C2N=CN=C12
11 248.0778156001 [NH3+]C1=C2N=CN(C3CC(O)C(=C=O)O3)C2=NC=N1
12 136.0617716121 [NH2+]=C1N=CN=C2NCN=C12
13 250.0934656641 [NH3+]C1=C2N=CN(C3CC(O)C(=CO)O3)C2=NC=N1
14 252.1091157281 [NH3+]C1=C2N=CN(C3CC(O)C(CO)O3)C2=NC=N1
15 234.0985510441 C=C1OC(N2C=NC3=C([NH3+])N=CN=C32)CC1O
16 234.0985510441 [NH3+]C1=C2N=CN(C3CCC(=CO)O3)C2=NC=N1
17 252.1091157281 [NH3+]C1=C2N=CN(C(=O)CC(O)CCO)C2=NC=N1 Intermediate Fragment 
18 234.0985510441 [NH3+]C1=C2N=CN(CCC(O)C#CO)C2=NC=N1
19 75.0440558841 OCC=C[OH2+]
20 254.1247657921 [NH3+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
21 256.1404158561 [NH3+]C1N=CN=C2C1NCN2C1CC(O)C(CO)O1
22 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)CC(O)=C3O)C2=C1C=CC=C2
23 424.1768013641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)CC(O)=C3O)C2=C1C=CC=C2
24 426.1924514281 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)CC(O)=C3O)C2=C1CCC=C2
25 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)C(=O)C=C3O)C2=C1CCC=C2
26 424.1768013641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C=C3O)C2=C1CCC=C2
27 426.1924514281 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C=C3O)C2=C1CCCC2
28 522.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CCC(CO)O5)C4=NC=N2)C(=O)C=C3O)C2=C1C=CC=C2
29 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCC(O)C(O)CO)C4=NC=N2)C(=O)C=C3O)C2=C1C=CC=C2 Intermediate Fragment 
30 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)C(=O)C(O)=C3)C2=C1CCC=C2
31 293.1536063321 CC1=C(C)C2CCCCC2C2=C1C1=CC(=[OH+])C(=O)C=C1C=C2
32 289.1223062041 CC1=C(C)C2=C(C=CC3=CC(=O)C(=[OH+])C=C32)C2=C1CCC=C2
33 522.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)C(=O)C=C3)C2=C1C=CC=C2
34 422.1611513001 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2
35 424.1768013641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3)C2=C1CCC=C2
36 426.1924514281 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3)C2=C1CCCC2
37 428.2081014921 CC1=C(C)C2CCCCC2C2=C1C1=C(C=C2)C=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
38 430.2237515561 CC1C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3)C2CCCCC2C1C
39 510.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)CO5)C4=NC=N2)C(=O)C(O)=C3)C2=C1CCC=C2
40 522.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(C)O5)C4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2
41 522.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CCC(CO)O5)C4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2
42 540.2241454801 CC1=C(C)C2=C(C=CC(C=C=O)=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCC=C2 Intermediate Fragment 
43 510.2135807961 CC1=C(C(C=O)=[NH+]C2=C3N=CN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C(C=C1)C1=C(C=CC=C1)C(C)=C2C
44 512.2292308601 CC1=C(C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C(C=C1)C1=C(C=CC=C1)C(C)=C2C
45 512.2292308601 CC1=C(C)C2=C(C=CC(=C=C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2
46 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCC(O)C(O)CO)C4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2 Intermediate Fragment 
47 522.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4N=CN(CCC(O)CCO)C4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2
48 542.2034100361 CC1=CC2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
49 119.0702706321 OC1CCOC1C[OH2+]
50 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
51 117.0546205681 OC1CCOC1=C[OH2+]
52 99.0440558841 C=C1OCC=C1[OH2+]
53 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
54 99.0440558841 CC=C(O)C#C[OH2+]
55 117.0546205681 O=CCC(O)C=C[OH2+] Intermediate Fragment 
56 115.0389705041 OC1CCOC1=C=[OH+]
57 111.0076703761 O=C1C=COC1=C=[OH+]
58 440.1717159841 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
59 442.1873660481 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
60 322.1437699201 CC1=C(C)C2=C(C=CC3=C2C(=[NH2+])C(=O)C(O)=C3O)C2=C1CCCC2
61 320.1281198561 CC1=C(C)C2=C(C=CC3=C2C(=[NH2+])C(=O)C(O)=C3O)C2=C1CCC=C2
62 121.0508725801 C1=NC2=NC=[NH+]CC2=N1
63 305.1172208241 CC1=C(C)C2=C(C=CC3=C(O)C(=O)C(=[OH+])C=C32)C2=C1CCC=C2
64 138.0774216761 [NH2+]=C1N=CN=C2NCNC12
65 140.0930717401 [NH3+]C1N=CN=C2NCNC21
66 426.1560659201 CC1=CC2=C(C=CCC2)C2=C1C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
67 426.1560659201 CC1=CC2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
68 442.1873660481 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CC4=C(N=C=N)NCN4)=C32)C2=C1CCC=C2 Intermediate Fragment 
69 425.1608169521 C=[NH+]C1=C(C=NC2=C3C(=C(O)C(O)=C2O)C=CC2=C3C(C)=C(C)C3=C2C=CC=C3)N=CN1
70 415.1764670161 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C=C4NCNC4=[NH2+])=C32)C2=C1CCC=C2
71 400.1655679841 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CC4=CNCN4)=C32)C2=C1C=CC=C2
72 398.1499179201 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CC4=CNC=N4)=C32)C2=C1C=CC=C2
73 442.1873660481 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CN=C=NC4=CNCN4)=C32)C2=C1CCC=C2 Intermediate Fragment 
74 349.1546689521 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C[NH3+])=C32)C2=C1CCC=C2
75 361.1546689521 C=[NH+]C=NC1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1C=CCC2
76 372.1342678561 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C[NH+]=C=N)=C32)C2=C1C=CC=C2
77 374.1499179201 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C[NH+]=C=N)=C32)C2=C1CCC=C2
78 107.0352225161 [NH+]#CN=C1C=NC=N1
79 332.1281198561 C=[NH+]C1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1C=CC=C2
80 334.1437699201 C=[NH+]C1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1C=CCC2
81 336.1594199841 C=[NH+]C1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1CCCC2
82 440.1717159841 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C[NH+]=C=NC4=CNCN4)=C32)C2=C1C=CC=C2
83 442.1873660481 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=C(N=C=N)C4=CNCN4)=C32)C2=C1CCC=C2 Intermediate Fragment 
84 415.1764670161 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C([NH3+])C4=CNCN4)=C32)C2=C1C=CC=C2
85 425.1608169521 C=[NH+]C(=NC1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1C=CC=C2)C1=CNC=N1
86 442.1873660481 C=NC([NH+]=C1C(=O)C(O)=C(O)C2=C1C1=C(C=C2)C2=C(CCC=C2)C(C)=C1C)=C1NCNC1=N Intermediate Fragment 
87 413.1608169521 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C=C4NCNC4=[NH2+])=C32)C2=C1C=CC=C2
88 442.1873660481 C=NC1=C(C(N)=[NH+]C2=C3C(=C(O)C(O)=C2O)C=CC2=C3C(C)=C(C)C3=C2C=CC=C3)NCN1 Intermediate Fragment 
89 413.1608169521 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C([NH3+])C4=CNC=N4)=C32)C2=C1C=CC=C2
90 440.1717159841 C=NC1=C(C([NH3+])=NC2=C3C(=C(O)C(O)=C2O)C=CC2=C3C(C)=C(C)C3=C2C=CC=C3)N=CN1
91 442.1873660481 CC1=C(C)C2=C(C=CC(C=O)=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(O)=C=O)C2=C1CCCC2 Intermediate Fragment 
92 235.1117415201 C=C1C2=C(C=CC=C2)C2=C(C=C(C=[OH+])C=C2)C1C
93 233.0960914561 C=C1C(=C)C2=C(C=CC(C=[OH+])=C2)C2=C1C=CC=C2
94 412.1768013641 CC1=C(C)C2=C(C=CC(C=O)=C2C(C=O)=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCC=C2
95 382.1662366801 CC1=C(C)C2=C(C=CC(=C=O)C2=C=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCC=C2
96 380.1505866161 CC1=C(C)C2=C(C=CC(=C=O)C2=C=[NH+]C2=C3NCNC3=NC=N2)C2=C1C=CC=C2
97 442.1873660481 CC1=C(C)C2=C(C=CC(C(O)=C=O)=C2C(C=O)=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCCC2 Intermediate Fragment 
98 267.1379562681 CC1C2=C(C=CC(C(O)=C=[OH+])=C2)C2=C(CCC=C2)C1C
99 265.1223062041 CC1C2=C(C=CC=C2)C2=C(C=C(C(O)=C=[OH+])C=C2)C1C
100 63.0440558841 OCC[OH2+]
101 380.1505866161 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3N=CNC3=NC=N2)C2=C1C=CC=C2
102 382.1662366801 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCNC3=NC=N2)C2=C1C=CC=C2
103 384.1818867441 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCC=C2
104 386.1975368081 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCCC2
105 412.1768013641 CC1=C(C)C2=C(C=CC(=C(O)C=O)C2=C=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCC=C2
106 410.1611513001 CC1=C(C)C2=C(C=CC(=C(O)C=O)C2=C=[NH+]C2=C3NCNC3=NC=N2)C2=C1C=CC=C2
107 442.1873660481 CC1=C(C)C2=C(C=CC(=C(O)C(O)=C=O)C2=C=[NH+]C2=C3NCNC3=NC=N2)C2=C1CCCC2 Intermediate Fragment 
108 442.1873660481 CC1=C(C)C2=C(C=CCC2)C2=C1C=C(C(O)=C(O)C(=O)C=[NH+]C1=C3NCNC3=NC=N1)C=C2 Intermediate Fragment 
109 442.1873660481 CC1=C(C)C2=C(C=CC=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C=O)C2=C1CCCC2 Intermediate Fragment 
110 207.1168269001 CC1C(=[CH3+])C2=C(C=CC=C2)C2=C1C=CC=C2
111 352.1556719961 CC1=C2CC=CC=C2C2=CC=CC(=C=[NH+]C3=C4N=CNC4=NC=N3)C2=C1C
112 442.1873660481 C=CC1=C(C2=CC3=C(CCC=C3)C(C)=C2C)C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
113 442.1873660481 CC1=CC(=C=CC2=CC(=[NH+]C3=C4NCNC4=NC=N3)C(=O)C(O)=C2O)C2=CCCCC2=C1C Intermediate Fragment 
114 442.1873660481 C=C=C1C2=CCCCC2=C(C)C(C)=C1C1=CC(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1 Intermediate Fragment 
115 442.1873660481 CC=C1CC=CC=C1C1=C(CC)C2=C(C=C1)C(O)=C(O)C(=O)C2=[NH+]C1=C2NCNC2=NC=N1 Intermediate Fragment 
116 416.1717159841 CCC1=C(C2=CCCC=C2)C=CC2=C1C(=[NH+]C1=C3NCNC3=NC=N1)C(=O)C(O)=C2O
117 442.1873660481 CC=C(C)C1=C(C2=CCCC=C2)C=CC2=C1C(=[NH+]C1=C3NCNC3=NC=N1)C(=O)C(O)=C2O Intermediate Fragment 
118 388.1404158561 O=C1C(=[NH+]C2=C3NCNC3=NC=N2)C2=C(C=CC(C3=CCCC=C3)=C2)C(O)=C1O
119 362.1247657921 C=C=C(C)C1=CC=CC2=C1C(=[NH+]C1=C3NCNC3=NC=N1)C(=O)C(O)=C2O
120 360.1091157281 C=C=C(C)C1=CC=CC2=C1C(=[NH+]C1=C3N=CNC3=NC=N1)C(=O)C(O)=C2O
121 442.1873660481 CC(C1=CC=CCC1)=C(C)C1=CC=CC2=C1C(=[NH+]C1=C3NCNC3=NC=N1)C(=O)C(O)=C2O Intermediate Fragment 
122 442.1873660481 CC=C(C)C1=C(C2=CC3=C(C=C2)C(O)=C(O)C(=O)C3=[NH+]C2=C3NCNC3=NC=N2)C=CCC1 Intermediate Fragment 
123 442.1873660481 C=CCC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1 Intermediate Fragment 
124 426.1560659201 C#CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
125 416.1717159841 CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
126 414.1560659201 CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2N=CNC2=NC=N1
127 402.1560659201 CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O
128 398.1247657921 [CH2+]C1=CC(C)=C(C)C2=C1C=CC1=C2C(=NC2=C3N=CNC3=NC=N2)C(=O)C(O)=C1O
129 442.1873660481 C=CC1=C(CC)C(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
130 412.1404158561 C#CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O
131 442.1873660481 CC=CC1=C(C)C(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
132 442.1873660481 CCC=CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
133 386.1247657921 CC1=C(C)C2=C(C=C1)C=CC1=C2C(=[NH+]C2=C3N=CNC3=NC=N2)C(=O)C(O)=C1O
134 388.1404158561 CC1=C(C)C2=C(C=C1)C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O
135 390.1560659201 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3O)CC1
136 400.1404158561 CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3N=CNC3=NC=N2)C(=O)C(O)=C1O
137 416.1717159841 CCC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCNC3=NC=N2)C(=O)C(O)=C1O
138 442.1873660481 C=CCCC1=CC2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1 Intermediate Fragment 
139 400.1404158561 CC1=CC2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2N=CNC2=NC=N1
140 442.1873660481 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC(NCN)=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
141 400.1655679841 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
142 398.1499179201 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
143 442.1873660481 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C(NCN)C=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
144 415.1764670161 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C(N)C=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
145 425.1608169521 CNC1=C([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(C=CC=C3)C(C)=C2C)N=CN=C1
146 442.1873660481 CNC1=C([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(CCC=C3)C(C)=C2C)N=CN=C1N Intermediate Fragment 
147 411.1451668881 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC(N)=NC=N2)C(=O)C(O)=C3O)C2=C1C=CC=C2
148 413.1608169521 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC(N)=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
149 415.1764670161 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC(N)=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
150 440.1717159841 CNC1=C([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(C=CC=C3)C(C)=C2C)N=CN=C1N
151 442.1873660481 CNC1=NC=NC([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(CCC=C3)C(C)=C2C)=C1N Intermediate Fragment 
152 425.1608169521 CNC1=NC=NC([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(C=CC=C3)C(C)=C2C)=C1
153 444.2030161121 CC1=C(C)C2CCCCC2C2=C1C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
154 446.2186661761 CC1C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C(O)=C3O)C2CCCCC2C1C
155 448.2343162401 CC1C(C)C2C3=C(C=CC2C2CCCCC12)C(O)=C(O)C(=O)C3=[NH+]C1=C2NCNC2=NC=N1
156 430.2237515561 CC1C2=C(C=CC3=C2C(=[NH+]C2=C4NCNC4=NC=N2)C(=O)C=C3O)C2CCCCC2C1C
157 448.2343162401 CC1C(C)C2C(C(=[NH+]C3=C4NCNC4=NC=N3)C(=O)C(O)=C=O)=CC=CC2C2CCCCC12 Intermediate Fragment 
158 526.2084954161 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)CO5)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
159 528.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)CO5)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
160 530.2397955441 CC1=C(C)C2CCCCC2C2=C1C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C3CC(O)CO3)C2=NC=N1
161 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(C)O5)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
162 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CCC(CO)O5)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
163 558.2347101641 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CC4=C(N=C=N)N(C5CC(O)C(CO)O5)CN4)=C32)C2=C1CCC=C2 Intermediate Fragment 
164 541.2081610681 C=[NH+]C1=C(C=NC2=C3C(=C(O)C(O)=C2O)C=CC2=C3C(C)=C(C)C3=C2C=CC=C3)N=CN1C1CC(O)C(CO)O1
165 531.2238111321 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C(N=C=C4NCN(C5CC(O)C(CO)O5)C4=[NH2+])=C32)C2=C1CCC=C2
166 558.2347101641 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=CN=C=NC4=CNCN4C4CC(O)C(CO)O4)=C32)C2=C1CCC=C2 Intermediate Fragment 
167 558.2347101641 CC1=C(C)C2=C(C=CC3=C(O)C(O)=C(O)C([NH+]=C(N=C=N)C4=CN(C5CC(O)C(CO)O5)CN4)=C32)C2=C1CCC=C2 Intermediate Fragment 
168 541.2081610681 C=[NH+]C(=NC1=C2C(=C(O)C(O)=C1O)C=CC1=C2C(C)=C(C)C2=C1C=CC=C2)C1=CN(C2CC(O)C(CO)O2)C=N1
169 558.2347101641 C=NC1=C(C(N)=[NH+]C2=C3C(=C(O)C(O)=C2O)C=CC2=C3C(C)=C(C)C3=C2C=CC=C3)NCN1C1CC(O)C(CO)O1 Intermediate Fragment 
170 558.2347101641 CC1=C(C)C2=C(C=CC(C=O)=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(O)=C=O)C2=C1CCCC2 Intermediate Fragment 
171 324.1302450961 OC=C(O)C=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
172 530.2397955441 CC1=C(C)C2=C(C=CC(C=O)=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2
173 528.2241454801 CC1=C(C)C2=C(C=CC(C=O)=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCC=C2
174 500.2292308601 CC1=C(C)C2=C(C=CC(=C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2
175 498.2135807961 CC1=C(C)C2=C(C=CC(=C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCC=C2
176 496.1979307321 CC1=C(C)C2=C(C=CC(=C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1C=CC=C2
177 558.2347101641 CC1=C(C)C2=C(C=CC(C(O)=C=O)=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2 Intermediate Fragment 
178 496.1979307321 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3N=CN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1C=CC=C2
179 468.2030161121 CC1=C2CC=CC=C2C2=CC=CC(=C=[NH+]C3=C4N=CN(C5CC(O)C(CO)O5)C4=NC=N3)C2=C1C
180 498.2135807961 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1C=CC=C2
181 500.2292308601 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCC=C2
182 502.2448809241 CC1=C(C)C2=C(C=CC=C2C(C=O)=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2
183 530.2397955441 CC1=C(C)C2=C(C=CC(=C(O)C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2
184 528.2241454801 CC1=C(C)C2=C(C=CC(=C(O)C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCC=C2
185 526.2084954161 CC1=C(C)C2=C(C=CC(=C(O)C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1C=CC=C2
186 558.2347101641 CC1=C(C)C2=C(C=CC(=C(O)C(O)=C=O)C2=C=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C1CCCC2 Intermediate Fragment 
187 89.0233204401 OC=C(O)C=[OH+]
188 470.2186661761 CC1=C2CC=CC=C2C2=CC=CC(=C=[NH+]C3=C4NCN(C5CC(O)C(CO)O5)C4=NC=N3)C2=C1C
189 474.2499663041 CC1=C2C(=C=[NH+]C3=C4NCN(C5CC(O)C(CO)O5)C4=NC=N3)C=CC=C2C2CCCCC2=C1C
190 558.2347101641 CC1=C(C)C2=C(C=CCC2)C2=C1C=C(C(O)=C(O)C(=O)C=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C=C2 Intermediate Fragment 
191 558.2347101641 CC1=C(C)C2=C(C=CC=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C=O)C2=C1CCCC2 Intermediate Fragment 
192 352.1251597161 O=C(C=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1)C(O)=CO
193 354.1408097801 OC=C(O)C(O)C=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
194 558.2347101641 CC=C1CC=CC=C1C1=C(CC)C2=C(C=C1)C(O)=C(O)C(=O)C2=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1 Intermediate Fragment 
195 532.2190601001 CCC1=C(C2=CCCC=C2)C=CC2=C1C(=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C(=O)C(O)=C2O
196 558.2347101641 CC=C(C)C1=C(C2=CCCC=C2)C=CC2=C1C(=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C(=O)C(O)=C2O Intermediate Fragment 
197 504.1877599721 O=C1C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C2=C(C=CC(C3=CCCC=C3)=C2)C(O)=C1O
198 528.1877599721 C=CC1=C(C2=CC=CC=C2)C=CC2=C1C(=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C(=O)C(O)=C2O
199 478.1721099081 C=C=C(C)C1=CC=CC2=C1C(=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C(=O)C(O)=C2O
200 366.1560659201 CCC(C)C1=CC=CC2=C1C(=[NH+]C1=C3NCNC3=NC=N1)C(=O)C(O)=C2O
201 558.2347101641 CC(C1=CC=CCC1)=C(C)C1=CC=CC2=C1C(=[NH+]C1=C3NCN(C4CC(O)C(CO)O4)C3=NC=N1)C(=O)C(O)=C2O Intermediate Fragment 
202 558.2347101641 CC=C(C)C1=C(C2=CC3=C(C=C2)C(O)=C(O)C(=O)C3=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C=CCC1 Intermediate Fragment 
203 558.2347101641 C=CCC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1 Intermediate Fragment 
204 542.2034100361 C#CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
205 430.1873660481 CCC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCNC2=NC=N1
206 532.2190601001 CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1
207 530.2034100361 CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2N=CN(C3CC(O)C(CO)O3)C2=NC=N1
208 528.1877599721 CC1=C(C)C2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2N=CN(C3CC(O)C(=CO)O3)C2=NC=N1
209 514.1721099081 CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3N=CN(C4CC(O)C(=CO)O4)C3=NC=N2)C(=O)C(O)=C1O
210 558.2347101641 C=CC1=C(CC)C(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
211 558.2347101641 CC=CC1=C(C)C(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
212 558.2347101641 CCC=CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O Intermediate Fragment 
213 504.1877599721 CC1=C(C)C2=C(C=C1)C=CC1=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O
214 506.2034100361 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C5CC(O)C(CO)O5)C4=NC=N2)C(=O)C(O)=C3O)CC1
215 508.2190601001 CC1CCC2=C(C3=C(C=C2)C(O)=C(O)C(=O)C3=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C1C
216 516.1877599721 CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3N=CN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O
217 518.2034100361 CC1=CC(C)=C(C)C2=C1C=CC1=C2C(=[NH+]C2=C3NCN(C4CC(O)C(CO)O4)C3=NC=N2)C(=O)C(O)=C1O
218 558.2347101641 C=CCCC1=CC2=C(C(C)=C1C)C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C3CC(O)C(CO)O3)C2=NC=N1 Intermediate Fragment 
219 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=CC(N(CN)C4CC(O)C(CO)O4)=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
220 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C(NCNC4CC(O)C(CO)O4)C=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
221 558.2347101641 CNC1=C([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(CCC=C3)C(C)=C2C)N=CN=C1NC1CC(O)C(CO)O1 Intermediate Fragment 
222 427.1764670161 CNC1=C([NH+]=C2C(=O)C(O)=C(O)C3=C2C2=C(C=C3)C3=C(CCC=C3)C(C)=C2C)N=CN=C1
223 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCC(O)C(O)CO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
224 540.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCC(O)CCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1C=CC=C2
225 522.2135807961 CCC(O)CCN1C=NC2=C([NH+]=C3C(=O)C(O)=C(O)C4=C3C3=C(C=C4)C4=C(C=CC=C4)C(C)=C3C)N=CN=C21
226 502.2448809241 CC1=C(C)C2CCCCC2C2=C1C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(CCCO)C2=NC=N1
227 500.2292308601 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
228 498.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
229 496.1979307321 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CCCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1C=CC=C2
230 478.1873660481 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4N=CN(CCCO)C4=NC=N2)C(=O)C(O)=C3)C2=C1C=CC=C2
231 470.2186661761 CCN1CNC2=C([NH+]=C3C(=O)C(O)=C(O)C4=C3C3=C(C=C4)C4=C(CCCC4)C(C)=C3C)N=CN=C21
232 468.2030161121 CCN1CNC2=C([NH+]=C3C(=O)C(O)=C(O)C4=C3C3=C(C=C4)C4=C(CCC=C4)C(C)=C3C)N=CN=C21
233 466.1873660481 CCN1CNC2=C([NH+]=C3C(=O)C(O)=C(O)C4=C3C3=C(C=C4)C4=C(C=CC=C4)C(C)=C3C)N=CN=C21
234 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(C)OC(CO)CO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
235 484.1979307321 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(C)O)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
236 486.2135807961 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(C)O)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
237 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(CCO)OCCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
238 516.2241454801 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(O)CCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCCC2
239 558.2347101641 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(O)CC(O)CCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2 Intermediate Fragment 
240 514.2084954161 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(O)CCO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
241 490.2448809241 CC1C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(C(C)O)C4=NC=N2)C(=O)C(O)=C3O)C2CCCCC2C1C
242 488.2292308601 CC1=C(C)C2CCCCC2C2=C1C1=C(C=C2)C(O)=C(O)C(=O)C1=[NH+]C1=C2NCN(C(C)O)C2=NC=N1
243 470.1822806681 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1CCC=C2
244 468.1666306041 CC1=C(C)C2=C(C=CC3=C2C(=[NH+]C2=C4NCN(CO)C4=NC=N2)C(=O)C(O)=C3O)C2=C1C=CC=C2
245 89.0597059481 CC(O)C=C[OH2+]
