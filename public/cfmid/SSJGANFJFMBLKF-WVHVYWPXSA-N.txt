#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=N1(C(=O)N(C(C1O)O)C(=O)N)[C@@H]1O[C@H](CO)[C@@H](C1)O
#InChiKey=SSJGANFJFMBLKF-WVHVYWPXSA-N
energy0
117.05462 19.49 6 94 112 98 113 115 (3.6348 0.6738 0.48606 0.31696 0.17265 0.050876)
145.02438 19.37 46 149 (5.2856 0.017674)
147.04003 22.43 47 168 (6.1397 0.0020004)
217.08190 17.38 4 15 14 9 (2.6203 1.1611 0.55796 0.41847)
235.09246 50.87 8 30 21 16 17 18 22 23 (13.296 0.26869 0.23614 0.10748 0.0096061 0.0041928 0.0036208 0.0018466)
243.06116 19.87 52 53 42 81 41 43 (3.4218 0.95132 0.57113 0.36002 0.11558 0.020569)
260.08771 12.90 35 160 37 157 1 3 245 161 159 39 158 (1.6203 1.2176 0.18532 0.18322 0.13408 0.1151 0.055998 0.016696 0.0016272 0.00073751 0.00013158)
261.07173 100.00 40 80 79 57 89 54 68 90 78 87 (23.287 2.4153 1.1458 0.2144 0.16436 0.076311 0.027645 0.026798 0.011121 0.0077992)
278.09828 42.20 0 287 244 172 162 240 284 234 247 204 248 (11.055 0.27309 0.14152 0.04743 0.012202 0.0070706 0.0065988 0.0047392 0.0034231 0.0023981 0.00064709)
energy1
45.03349 17.06 109 (3.1374)
57.03349 15.18 102 (2.7914)
76.03930 10.34 134 145 (1.9008 0.00034472)
87.01890 7.85 127 (1.4432)
87.04406 9.24 33 (1.6992)
99.04406 50.29 99 96 97 (6.608 1.8241 0.81596)
102.01857 9.08 132 70 (1.6689 0.00030474)
117.02947 39.30 12 129 242 (6.3521 0.87147 0.0027007)
117.05462 30.72 6 98 94 112 113 115 (1.8085 1.7967 1.4909 0.41535 0.10253 0.035639)
119.04512 15.87 13 150 139 (2.5187 0.39353 0.0055398)
144.04037 73.39 2 121 125 120 188 (11.314 1.777 0.3036 0.089872 0.01092)
145.02438 100.00 46 149 (18.056 0.33221)
147.04003 18.15 47 168 (3.3225 0.015692)
162.05093 19.70 119 133 126 148 138 131 (2.4132 0.68669 0.22244 0.22105 0.058823 0.019364)
217.08190 10.64 4 14 9 15 (1.4655 0.26563 0.12222 0.10285)
243.06116 8.39 52 53 42 41 43 81 (1.1464 0.19945 0.10437 0.044015 0.040832 0.0074028)
energy2
44.01309 53.83 123 (9.9552)
71.02399 56.36 165 166 (5.7446 4.6785)
72.00800 25.69 144 (4.7518)
73.03964 100.00 215 216 (16.366 2.1291)
74.02365 5.84 72 (1.0798)
76.03930 5.19 134 145 (0.95458 0.0057264)
77.05971 5.06 32 (0.93674)
87.01890 6.81 127 (1.2592)
89.03455 4.81 128 (0.88903)
91.05020 5.07 142 (0.93829)
93.05462 5.34 24 (0.9869)
99.04406 10.18 96 97 99 (1.4881 0.33888 0.055878)
103.05020 5.89 175 174 (0.57897 0.51084)
104.03422 11.70 61 69 (2.1502 0.014352)
117.02947 6.44 12 129 242 (0.59588 0.59584 5.3566e-05)
117.05462 12.08 98 94 6 113 115 112 (0.68655 0.51349 0.40625 0.36183 0.13776 0.12816)
130.02472 10.42 189 (1.927)
132.04037 18.21 146 147 (3.3668 0.0010609)
132.06552 8.08 167 55 (1.421 0.073668)
144.04037 6.34 188 2 121 125 120 (0.56342 0.5123 0.085389 0.0089094 0.0030965)
158.04478 9.59 63 (1.7731)
160.06043 19.89 64 (3.6794)
162.07608 23.58 19 (4.3606)
190.08223 7.37 246 (1.3631)
202.08223 7.36 194 196 193 38 (0.51131 0.39908 0.35087 0.099636)
216.06150 5.68 178 (1.0498)

0 278.0982762721 [NH3+]C(=O)N1C(=O)N(C2CC(O)C(CO)O2)C(O)C1O
1 260.0877115881 [NH3+]C(=O)N1C(=O)N(C2CC(O)C(CO)O2)C=C1O
2 144.0403674721 [NH3+]C(=O)N1C(=O)CNC1=O
3 260.0877115881 [NH3+]C(=O)N1C=C(O)N(C2CC(O)C(CO)O2)C1=O
4 217.0818979361 OCC1OC(N2C(=[OH+])N=CC2O)CC1O
5 231.0611624921 O=C1C(O)=NC(=[OH+])N1C1CC(O)C(CO)O1
6 117.0546205681 OC1CCOC1=C[OH2+]
7 233.0768125561 OCC1OC(N2C(=[OH+])N=C(O)C2O)CC1O
8 235.0924626201 OCC1OC(N2C(=[OH+])NC(O)C2O)CC1O
9 217.0818979361 OCC1OC(N2CC(O)=NC2=[OH+])CC1O
10 119.0702706321 OC1CCOC1C[OH2+]
11 115.0389705041 OC1CCOC1=C=[OH+]
12 117.0294684401 OC1=NC([OH2+])=NC1O
13 119.0451185041 OC1N=C([OH2+])NC1O
14 217.0818979361 CC1OC(N2C(=[OH+])N=C(O)C2O)CC1O
15 217.0818979361 OCC1CCC(N2C(=[OH+])N=C(O)C2O)O1
16 235.0924626201 OCC1OC(NC(O)C(O)N=C=[OH+])CC1O Intermediate Fragment 
17 235.0924626201 OCC(O)=NC([OH2+])=NC1CC(O)C(CO)O1 Intermediate Fragment 
18 235.0924626201 OC=NC(=[OH+])N(CO)C1CC(O)C(CO)O1 Intermediate Fragment 
19 162.0760842841 O=C[NH2+]C1CC(O)C(CO)O1
20 164.0917343481 OC[NH2+]C1CC(O)C(CO)O1
21 235.0924626201 NC(=[OH+])N(C(O)=CO)C1CC(O)C(CO)O1 Intermediate Fragment 
22 235.0924626201 N=C(O)C(O)N(C=[OH+])C1CC(O)C(CO)O1 Intermediate Fragment 
23 235.0924626201 CC(OC(CO)CO)N1C(=[OH+])N=C(O)C1O Intermediate Fragment 
24 93.0546205681 OCC([OH2+])CO
25 143.0451185041 CCN1C(=O)C(O)=NC1=[OH+]
26 159.0400331241 CC(O)N1C(=O)C(O)=NC1=[OH+]
27 161.0556831881 CC(O)N1C(=[OH+])N=C(O)C1O
28 163.0713332521 CC(O)N1C(=[OH+])NC(O)C1O
29 165.0869833161 CC(O)N1C([OH2+])NC(O)C1O
30 235.0924626201 OCCC(O)CC(O)N1C(=[OH+])N=C(O)C1O Intermediate Fragment 
31 75.0440558841 OCC=C[OH2+]
32 77.0597059481 OCCC[OH2+]
33 87.0440558841 CC(O)C#C[OH2+]
34 89.0597059481 CC(O)C=C[OH2+]
35 260.0877115881 [NH2+]=CN1C(=O)N(C2CC(O)C(CO)O2)C(O)=C1O
36 146.0560175361 [NH3+]CN1C(O)=NC(O)=C1O
37 260.0877115881 [NH2+]=CN(C(O)=C=O)C(O)=NC1CC(O)C(CO)O1 Intermediate Fragment 
38 202.0822322841 N=C=NC(O)=[NH+]C1CC(O)C(CO)O1
39 260.0877115881 CC(OC(CO)CO)N1C(=O)N(C#[NH+])C(O)=C1O Intermediate Fragment 
40 261.0717271761 O=CN1C(=[OH+])N(C2CC(O)C(CO)O2)C(O)=C1O
41 243.0611624921 O=CN1C(=[OH+])N(C2CC(O)C(=CO)O2)C=C1O
42 243.0611624921 O=CN1C=C(O)N(C2CC(O)C(=CO)O2)C1=[OH+]
43 243.0611624921 CN1C(=[OH+])N(C2CC(O)C(=C=O)O2)C(O)=C1O
44 111.0076703761 O=C1C=COC1=C=[OH+]
45 143.0087329961 [O+]#CN1C(O)=NC(O)=C1O
46 145.0243830601 O=CN1C([OH2+])=NC(O)=C1O
47 147.0400331241 OCN1C([OH2+])=NC(O)=C1O
48 149.0556831881 OCN1C([OH2+])=NC(O)C1O
49 151.0713332521 OCN1C([OH2+])NC(O)C1O
50 231.0611624921 O=CN1C(=[OH+])N(C2CC(O)CO2)C(O)=C1O
51 233.0768125561 OCN1C(=[OH+])N(C2CC(O)CO2)C(O)=C1O
52 243.0611624921 C=C1OC(N2C(=[OH+])N(C=O)C(O)=C2O)CC1O
53 243.0611624921 O=CN1C(=[OH+])N(C2CCC(=CO)O2)C(O)=C1O
54 261.0717271761 O=CN(C=[OH+])C(O)=C(O)N=C1CC(O)C(CO)O1 Intermediate Fragment 
55 132.0655196001 [NH2+]=C1CC(O)C(CO)O1
56 130.0134840281 O=C=C(O)[NH+](C=O)C=O
57 261.0717271761 O=C=C(O)N(C=O)C([OH2+])=NC1CC(O)C(CO)O1 Intermediate Fragment 
58 128.0342194721 [NH2+]=C1CC(O)C(=C=O)O1
59 130.0498695361 [NH2+]=C1CC(O)C(=CO)O1
60 134.0811696641 [NH3+]C1CC(O)C(CO)O1
61 104.0342194721 O=CC(=O)[NH2+]CO
62 156.0291340921 O=C=[NH+]C1CC(O)C(=C=O)O1
63 158.0447841561 O=C=[NH+]C1CC(O)C(=CO)O1
64 160.0604342201 O=C=[NH+]C1CC(O)C(CO)O1
65 201.0505978081 O=C=NC(O)=[NH+]C1CC(O)C(=CO)O1
66 203.0662478721 O=C=NC(O)=[NH+]C1CC(O)C(CO)O1
67 233.0768125561 O=CN(C=O)C(O)=[NH+]C1CC(O)C(CO)O1
68 261.0717271761 O=CN(C=O)C(=[OH+])N(C=O)C1CC(O)C(CO)O1 Intermediate Fragment 
69 104.0342194721 O=C[NH+](C=O)CO
70 102.0185694081 O=C[NH+](C=O)C=O
71 100.0029193441 O=C=[N+](C=O)C=O
72 74.0236547881 O=C=[NH+]CO
73 184.0240487121 O=C=C1OC([NH+](C=O)C=O)C=C1O
74 186.0396987761 O=C=C1OC([NH+](C=O)C=O)CC1O
75 188.0553488401 O=C[NH+](C=O)C1CC(O)C(=CO)O1
76 190.0709989041 O=C[NH+](C=O)C1CC(O)C(CO)O1
77 233.0768125561 O=C[NH+](C(=O)N=CO)C1CC(O)C(CO)O1
78 261.0717271761 O=C=C(O)N(C(=[OH+])N=CO)C1CC(O)C(CO)O1 Intermediate Fragment 
79 261.0717271761 O=CN=C(O)C(=O)N(C=[OH+])C1CC(O)C(CO)O1 Intermediate Fragment 
80 261.0717271761 O=CN1C(=[OH+])N(CCC(O)C(=O)CO)C(O)=C1O Intermediate Fragment 
81 243.0611624921 O=CN1C(=[OH+])N(CCC(O)C#CO)C(O)=C1O
82 201.0505978081 O=CN1C(=[OH+])N(CC=CO)C(O)=C1O
83 173.0556831881 CCN1C(=[OH+])N(C=O)C(O)=C1O
84 171.0400331241 C=CN1C(=[OH+])N(C=O)C(O)=C1O
85 91.0389705041 OCC(=[OH+])CO
86 157.0243830601 CN1C(=O)C(=O)N(C=O)C1=[OH+]
87 261.0717271761 CC(OC(=CO)CO)N1C(=[OH+])N(C=O)C(O)=C1O Intermediate Fragment 
88 189.0505978081 CC(O)N1C(=[OH+])N(C=O)C(O)=C1O
89 261.0717271761 O=CN1C(=[OH+])N(C(CCO)OC=CO)C(O)=C1O Intermediate Fragment 
90 261.0717271761 O=CN1C(=[OH+])N(C(=O)CC(O)CCO)C(O)=C1O Intermediate Fragment 
91 101.0597059481 C=C1OCCC1[OH2+]
92 101.0597059481 [OH2+]C=C1CCCO1
93 119.0702706321 O=CCC(O)CC[OH2+] Intermediate Fragment 
94 117.0546205681 OC=CC(O)CC=[OH+]
95 85.0284058201 [OH2+]C1=COC=C1
96 99.0440558841 C=C1OCC=C1[OH2+]
97 99.0440558841 [OH+]=C=C1CCCO1
98 117.0546205681 CCC(O)C(=O)C=[OH+] Intermediate Fragment 
99 99.0440558841 CC=C(O)C#C[OH2+]
100 61.0647913281 CCC[OH2+]
101 59.0491412641 CC=C[OH2+]
102 57.0334912001 CC#C[OH2+]
103 59.0127557561 OC=C=[OH+]
104 61.0284058201 OCC=[OH+]
105 31.0542266441 C[CH4+]
106 29.0385765801 C=[CH3+]
107 73.0284058201 OC=CC=[OH+]
108 71.0127557561 OC#CC=[OH+]
109 45.0334912001 C=C[OH2+]
110 47.0491412641 CC[OH2+]
111 43.0178411361 C=C=[OH+]
112 117.0546205681 OCCCOC#C[OH2+] Intermediate Fragment 
113 117.0546205681 CCOC(=C=[OH+])CO Intermediate Fragment 
114 89.0597059481 CC[OH+]C=CO
115 117.0546205681 COC(=C=[OH+])C(C)O Intermediate Fragment 
116 115.0389705041 CC=C(O)C(=O)C=[OH+] Intermediate Fragment 
117 115.0389705041 O=CCC(O)C#C[OH2+] Intermediate Fragment 
118 160.0352820921 [NH3+]C(=O)N1C(O)=NC(O)=C1O
119 162.0509321561 [NH3+]C(O)N1C(O)=NC(O)=C1O
120 144.0403674721 [NH3+]C(=O)N1C=NC(O)=C1O
121 144.0403674721 [NH3+]C(=O)N1C=C(O)N=C1O
122 48.0443902321 [NH3+]CO
123 44.0130901041 [NH2+]=C=O
124 115.0138183761 O=C1N=C([OH2+])N=C1O
125 144.0403674721 [NH2+]=CN1C(O)=NC(O)=C1O
126 162.0509321561 NC(O)=C(O)N(C=O)C([NH3+])=O Intermediate Fragment 
127 87.0189037561 NC(=O)[NH+]=C=O
128 89.0345538201 NC(O)[NH+]=C=O
129 117.0294684401 NC(=O)[NH+](C=O)C=O
130 160.0352820921 NC(=O)N(C#[O+])C(O)=C(N)O
131 162.0509321561 [NH3+]C(O)N=C(O)C(O)N=C=O Intermediate Fragment 
132 102.0185694081 O=C=[NH+]C(O)=CO
133 162.0509321561 [NH3+]C(O)NC(O)=NC(O)=C=O Intermediate Fragment 
134 76.0393048521 [NH3+]C(=O)CO
135 61.0396392001 NC(=[NH2+])O
136 100.0029193441 O=C=[NH+]C(O)=C=O
137 104.0454528521 NC(O)=NC(=[NH2+])O
138 162.0509321561 [NH3+]C(O)N(C=O)C(O)=NC=O Intermediate Fragment 
139 119.0451185041 NC(O)[NH+](C=O)C=O
140 115.0138183761 NC(=O)[N+](=C=O)C=O
141 46.0287401681 [NH3+]C=O
142 91.0502038841 NC(O)[NH+]=CO
143 69.9923546601 O=C=NC#[O+]
144 72.0080047241 O=C=[NH+]C=O
145 76.0393048521 OC=[NH+]CO
146 132.0403674721 NC(O)=NC(O)=[NH+]C=O
147 132.0403674721 NC(=O)N(C=O)C(=[NH2+])O
148 162.0509321561 N=C(O)N(C(O)=C=O)C([NH3+])O Intermediate Fragment 
149 145.0243830601 NC(=O)[NH+](C=O)C(O)=C=O
150 119.0451185041 NC(O)[NH+]=C(O)C=O
151 164.0665822201 [NH3+]C(O)N1C(O)=NC(O)C1O
152 166.0822322841 [NH3+]C(O)N1C(O)NC(O)C1O
153 166.0822322841 [NH3+]C(O)NC(O)=NC(O)CO Intermediate Fragment 
154 246.0720615241 [NH3+]C(=O)N1C(=O)N(C2CC(O)CO2)C(O)=C1O
155 248.0877115881 [NH3+]C(O)N1C(=O)N(C2CC(O)CO2)C(O)=C1O
156 248.0877115881 [NH3+]C(O)N(C=O)C(=O)N(C=O)C1CC(O)CO1 Intermediate Fragment 
157 260.0877115881 CC1OC(N2C(=O)N(C([NH3+])=O)C(O)=C2O)CC1O
158 260.0877115881 CC1OC(N(C=O)C(=O)N(C=O)C([NH3+])=O)CC1O Intermediate Fragment 
159 260.0877115881 CC1OC(N(C=O)C(=O)C(O)=NC([NH3+])=O)CC1O Intermediate Fragment 
160 260.0877115881 [NH3+]C(=O)N1C(=O)N(C2CCC(CO)O2)C(O)=C1O
161 260.0877115881 [NH3+]C(=O)N=C(O)C(=O)N(C=O)C1CCC(CO)O1 Intermediate Fragment 
162 278.0982762721 [NH3+]C(=O)N(C=O)C(O)=C(O)NC1CC(O)C(CO)O1 Intermediate Fragment 
163 250.1033616521 NC(O)N=C(O)C(=O)[NH2+]C1CC(O)C(CO)O1
164 190.0709989041 O=C=C(O)[NH2+]C1CC(O)C(CO)O1
165 71.0239891361 C#[N+]C(N)=O
166 71.0239891361 N=C[NH+]=C=O
167 132.0655196001 [NH2+]=C(O)CC(O)C=CO Intermediate Fragment 
168 147.0400331241 NC(O)[NH+](C=O)C(O)=C=O
169 149.0556831881 NC(O)[NH+](C=O)C(O)=CO
170 131.0451185041 C[NH+](C(N)=O)C(O)=C=O
171 131.0451185041 C=C(O)[NH+](C=O)C(N)=O
172 278.0982762721 [NH3+]C(O)N(C(O)=C=O)C(O)=NC1CC(O)C(CO)O1 Intermediate Fragment 
173 121.0607685681 NC(O)[NH+]=C(O)CO
174 103.0502038841 CC(O)=[NH+]C(N)=O
175 103.0502038841 NC(O)[NH+]=C=CO
176 101.0345538201 [CH2+]C(O)=NC(N)=O
177 101.0345538201 NC(O)[NH+]=C=C=O
178 216.0614968401 NC(O)=NC(O)=[NH+]C1CC(O)C(=C=O)O1
179 218.0771469041 NC(O)=NC(O)=[NH+]C1CC(O)C(=CO)O1
180 175.0713332521 NC(O)=[NH+]C1CC(O)C(=CO)O1
181 108.0767529801 NC(O)NC([NH3+])O
182 200.0665822201 C=C1OC([NH+]=C(O)N=C(N)O)C=C1O
183 200.0665822201 NC(O)=NC(O)=[NH+]C1CCC(=C=O)O1
184 218.0771469041 NC(O)=NC(O)=[NH+]CCC(O)C(=O)C=O Intermediate Fragment 
185 218.0771469041 NC(O)=NC(O)=[NH+]C(=O)CC(O)C=CO Intermediate Fragment 
186 174.0509321561 NC(O)=NC(O)=[NH+]C(=O)C=CO
187 146.0560175361 CC(=O)[NH+]=C(O)N=C(N)O
188 144.0403674721 [CH2+]C(=O)N=C(O)N=C(N)O
189 130.0247174081 NC(O)=NC(O)=[N+]=C=O
190 218.0771469041 NC(O)=NC(O)=[NH+]C(CCO)OC#CO Intermediate Fragment 
191 218.0771469041 CC([NH+]=C(O)N=C(N)O)OC(=C=O)CO Intermediate Fragment 
192 220.0927969681 NC(O)=NC(O)=[NH+]C1CC(O)C(CO)O1
193 202.0822322841 C=C1OC([NH+]=C(O)N=C(N)O)CC1O
194 202.0822322841 NC(O)=NC(O)=[NH+]C1CCC(=CO)O1
195 220.0927969681 NC(O)=NC(O)=[NH+]C(=O)CC(O)CCO Intermediate Fragment 
196 202.0822322841 NC(O)=NC(O)=[NH+]CCC(O)C#CO
197 220.0927969681 NC(O)=NC(O)=[NH+]C(CCO)OC=CO Intermediate Fragment 
198 176.0665822201 NC(O)=NC(O)=[NH+]C(=O)CCO
199 220.0927969681 CC([NH+]=C(O)N=C(N)O)OC(=CO)CO Intermediate Fragment 
200 222.1084470321 NC(O)NC(O)=[NH+]C1CC(O)C(CO)O1
201 248.0877115881 NC(=O)N(C=O)C(O)=[NH+]C1CC(O)C(CO)O1
202 248.0877115881 NC(=O)N(C=O)C(O)=[NH+]C(=O)CC(O)CCO Intermediate Fragment 
203 250.1033616521 NC(O)N(C=O)C(O)=[NH+]C1CC(O)C(CO)O1
204 278.0982762721 [NH3+]C(O)N(C=O)C(=O)N(C=O)C1CC(O)C(CO)O1 Intermediate Fragment 
205 123.0764186321 NC(O)[NH+](CO)CO
206 105.0658539481 C[NH+](C=O)C(N)O
207 121.0607685681 NC(O)[NH+](C=O)CO
208 101.0345538201 C=[N+](C=O)C(N)=O
209 146.0811696641 CC1OC([NH2+]C=O)CC1O
210 164.0917343481 O=C[NH2+]CCC(O)C(O)CO Intermediate Fragment 
211 146.0811696641 O=C[NH+]=CCC(O)CCO
212 164.0917343481 O=C[NH2+]C(O)CC(O)CCO Intermediate Fragment 
213 164.0917343481 CC(O)C(CO)OC[NH2+]C=O Intermediate Fragment 
214 93.0658539481 NC(O)[NH2+]CO
215 73.0396392001 C=[NH+]C(N)=O
216 73.0396392001 NC[NH+]=C=O
217 170.0447841561 C[NH+](C=O)C1C=C(O)C(=C=O)O1
218 172.0604342201 C[NH+](C=O)C1CC(O)C(=C=O)O1
219 192.0866489681 O=C[NH+](CO)C1CC(O)C(CO)O1
220 246.0720615241 NC(O)=NC(=O)[NH+](C=O)C1CC(O)C(=CO)O1
221 248.0877115881 NC(O)=NC(=O)[NH+](C=O)C1CC(O)C(CO)O1
222 205.0818979361 NC(=O)[NH+](C=O)C1CC(O)C(CO)O1
223 231.0611624921 O=C=NC(=O)[NH+](C=O)C1CC(O)C(CO)O1
224 134.0560175361 NC(O)NC(O)=[NH+]C=O
225 136.0716676001 NC(O)NC(O)=[NH+]CO
226 138.0873176641 NC(O)NC(O)[NH2+]CO
227 248.0877115881 NC(O)=NC(=O)[NH+](C=O)CCC(O)C(=O)CO Intermediate Fragment 
228 230.0771469041 NC(O)=NC(=O)[NH+](C=O)CCC(O)C#CO
229 186.0509321561 NC(O)=NC(=O)[NH+](C=O)CC#CO
230 160.0716676001 CC[NH+](C=O)C(=O)N=C(N)O
231 156.0403674721 C#C[NH+](C=O)C(=O)N=C(N)O
232 248.0877115881 NC(O)=NC(=O)[NH+](C=O)C(=O)CC(O)CCO Intermediate Fragment 
233 250.1033616521 NC(O)NC(=O)[NH+](C=O)C1CC(O)C(CO)O1
234 278.0982762721 [NH3+]C(O)NC(=O)N(C(O)=C=O)C1CC(O)C(CO)O1 Intermediate Fragment 
235 188.0553488401 O=C=C(O)[NH+]=C1CC(O)C(CO)O1
236 192.0866489681 OC=C(O)[NH2+]C1CC(O)C(CO)O1
237 192.0866489681 O=C=C(O)[NH2+]C(O)CC(O)CCO Intermediate Fragment 
238 216.0502634601 O=C=C(O)[NH+](C=O)C1CC(O)C(=CO)O1
239 218.0659135241 O=C=C(O)[NH+](C=O)C1CC(O)C(CO)O1
240 278.0982762721 [NH3+]C(O)N=C(O)C(=O)N(C=O)C1CC(O)C(CO)O1 Intermediate Fragment 
241 43.0290745161 [NH+]#CN
242 117.0294684401 NC(=O)[NH+]=C(O)C=O
243 123.0764186321 NC(O)[NH2+]C(O)CO
244 278.0982762721 [NH3+]C(=O)N1C(=O)N(CCC(O)C(O)CO)C(O)=C1O Intermediate Fragment 
245 260.0877115881 [NH3+]C(=O)N1C(=O)N(CCC(O)C=CO)C(O)=C1O
246 190.0822322841 CCN1C(=O)N(C([NH3+])O)C(O)=C1O
247 278.0982762721 CC(O)C(CO)OCN1C(=O)N(C([NH3+])=O)C(O)=C1O Intermediate Fragment 
248 278.0982762721 CC(OC(CO)CO)N1C(=O)N(C([NH3+])=O)C(O)=C1O Intermediate Fragment 
249 186.0509321561 C=CN1C(=O)N(C([NH3+])=O)C(O)=C1O
250 186.0509321561 C=CN(C=O)C(=O)C(O)=NC([NH3+])=O Intermediate Fragment 
251 186.0509321561 C=CN(C(=O)N=C([NH3+])O)C(O)=C=O Intermediate Fragment 
252 100.0393048521 CC=[NH+]C(O)=C=O
253 130.0611029161 C=C[NH+]=C(O)N=C(N)O
254 128.0454528521 C#C[NH+]=C(O)N=C(N)O
255 126.0298027881 [C+]#CN=C(O)N=C(N)O
256 186.0509321561 C=CN(C=O)C(=O)N(C=O)C([NH3+])=O Intermediate Fragment 
257 186.0509321561 C=CN=C(O)N(C([NH3+])=O)C(O)=C=O Intermediate Fragment 
258 188.0665822201 CCN1C(=O)N(C([NH3+])=O)C(O)=C1O
259 145.0607685681 CCN1C(=[OH+])N=C(O)C1O
260 188.0665822201 CC=NC(O)=C(O)N(C=O)C([NH3+])=O Intermediate Fragment 
261 188.0665822201 CCN(C=O)C(=O)C(O)=NC([NH3+])=O Intermediate Fragment 
262 188.0665822201 CCN(C(=O)N=C([NH3+])O)C(O)=C=O Intermediate Fragment 
263 102.0549549161 CC[NH2+]C(O)=C=O
264 188.0665822201 CCN(C=O)C(=O)N(C=O)C([NH3+])=O Intermediate Fragment 
265 188.0665822201 CCN=C(O)N(C([NH3+])=O)C(O)=C=O Intermediate Fragment 
266 192.0978823481 CCN1C(=O)N(C([NH3+])O)C(O)C1O
267 202.0458467761 CC(=O)N1C(=O)N(C([NH3+])=O)C(O)=C1O
268 204.0614968401 CC(O)N1C(=O)N(C([NH3+])=O)C(O)=C1O
269 187.0349477441 CC(=O)N1C(=[OH+])N(C=O)C(O)=C1O
270 204.0614968401 CC(O)=NC(O)=C(O)N(C=O)C([NH3+])=O Intermediate Fragment 
271 204.0614968401 CC(O)N(C=O)C(=O)C(O)=NC([NH3+])=O Intermediate Fragment 
272 204.0614968401 CC(O)N(C(=O)N=C([NH3+])O)C(O)=C=O Intermediate Fragment 
273 118.0498695361 CC(O)[NH2+]C(O)=C=O
274 204.0614968401 CC(O)N(C=O)C(=O)N(C=O)C([NH3+])=O Intermediate Fragment 
275 88.0393048521 CC(=O)[NH2+]C=O
276 204.0614968401 CC(O)N=C(O)N(C([NH3+])=O)C(O)=C=O Intermediate Fragment 
277 206.0771469041 CC(O)N1C(=O)N(C([NH3+])O)C(O)=C1O
278 206.0771469041 CC(O)N(C=O)C(=O)C(O)=NC([NH3+])O Intermediate Fragment 
279 206.0771469041 CC(O)N(C(=O)NC([NH3+])O)C(O)=C=O Intermediate Fragment 
280 206.0771469041 CC(O)N(C=O)C(=O)N(C=O)C([NH3+])O Intermediate Fragment 
281 206.0771469041 CC(O)N=C(O)N(C(O)=C=O)C([NH3+])O Intermediate Fragment 
282 208.0927969681 CC(O)N1C(=O)N(C([NH3+])O)C(O)C1O
283 210.1084470321 CC(O)N1C(O)C(O)N(C([NH3+])O)C1O
284 278.0982762721 [NH3+]C(=O)N1C(=O)N(C(CCO)OCCO)C(O)=C1O Intermediate Fragment 
285 234.0720615241 [NH3+]C(=O)N1C(=O)N(C(O)CCO)C(O)=C1O
286 236.0877115881 [NH3+]C(O)N1C(=O)N(C(O)CCO)C(O)=C1O
287 278.0982762721 [NH3+]C(=O)N1C(=O)N(C(O)CC(O)CCO)C(O)=C1O Intermediate Fragment 
288 232.0564114601 [NH3+]C(=O)N1C(=O)N(C(=O)CCO)C(O)=C1O
289 192.0614968401 [NH3+]C(O)N1C(=O)N(CO)C(O)=C1O
290 190.0458467761 [NH3+]C(=O)N1C(=O)N(CO)C(O)=C1O
291 190.0458467761 [NH3+]C(=O)N(C=O)C(=O)N(C=O)CO Intermediate Fragment 
292 188.0301967121 [NH3+]C(=O)N1C(=O)N(C=O)C(O)=C1O
