#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#SMILES=[C@H]1(C[C@@H](O[C@@H]1CO)n1c(c(c(=O)[nH]c1=O)C)Nc1cc2c(-c3c(C2)cccc3)cc1)O
#InChiKey=YPRAGNZGZNSXPE-VAMGGRTRSA-N
energy0
306.12370 71.96 5 52 67 80 89 61 90 87 43 83 84 93 73 85 221 (33.242 1.0404 0.23997 0.15345 0.10619 0.09711 0.070471 0.047547 0.036226 0.023512 0.013798 0.0069027 0.0012931 0.0007457 5.3665e-06)
422.17105 100.00 0 197 267 242 286 166 284 293 261 161 266 243 300 269 294 149 299 281 277 280 297 295 283 282 (47.334 0.68322 0.1962 0.12603 0.075887 0.072767 0.054103 0.034412 0.030732 0.030721 0.022021 0.019913 0.01826 0.013227 0.010019 0.00756 0.0053761 0.0050861 0.0035626 0.0032511 0.0027959 0.00072534 0.0006061 8.3727e-05)
energy1
87.04406 3.59 99 233 (1.3827 0.13467)
99.04406 5.35 8 115 111 (1.4023 0.5999 0.25806)
101.05971 3.03 124 9 122 (0.64672 0.49766 0.13406)
111.00767 3.28 94 (1.3833)
140.04545 2.62 42 (1.1053)
182.09643 5.03 10 129 131 128 (2.0728 0.025284 0.02231 0.0012546)
220.11208 4.32 68 (1.8219)
235.12297 5.51 46 (2.3237)
261.10224 14.89 48 57 54 (3.2609 2.8118 0.2139)
263.11789 7.29 58 53 49 (1.2447 1.1969 0.6359)
278.12879 4.21 50 65 (1.3935 0.38553)
289.09715 3.09 59 81 223 (1.03 0.27144 0.0042973)
304.10805 6.23 6 51 60 92 82 88 86 (1.7912 0.78408 0.028326 0.016287 0.0053255 0.0027236 0.00015883)
306.12370 100.00 5 67 43 52 80 90 61 73 87 93 89 83 84 85 221 (32.002 3.2139 2.8354 1.3458 1.1487 0.61453 0.52446 0.166 0.10237 0.073758 0.071969 0.060654 0.047448 0.0016162 0.0014189)
316.10805 3.97 174 175 182 178 (1.4467 0.11463 0.05969 0.053155)
318.12370 3.25 162 163 (1.3413 0.031524)
334.11862 8.21 213 216 225 224 218 219 (3.1518 0.13306 0.056164 0.043837 0.042552 0.036473)
404.16048 2.79 3 148 183 21 17 20 16 15 187 23 125 (0.7761 0.20768 0.074164 0.07371 0.019584 0.008855 0.006144 0.006084 0.0029884 0.00096614 0.00085757)
422.17105 3.59 0 197 166 267 149 161 243 242 286 293 266 284 277 261 300 294 299 281 280 269 295 297 283 282 (0.97607 0.35943 0.040977 0.038218 0.017403 0.015314 0.015299 0.0098163 0.0075951 0.0074011 0.0072743 0.0057239 0.0033432 0.0030365 0.0014434 0.0013565 0.00094679 0.00087538 0.00074399 0.00067428 0.00039081 0.00036028 2.8165e-05 6.7146e-06)
energy2
45.03349 20.35 106 (1.7252)
57.03349 46.25 104 (3.9211)
59.04914 28.77 116 (2.4389)
99.04406 11.77 8 111 115 (0.46707 0.45406 0.077002)
125.03455 100.00 25 (8.4772)
127.05020 65.40 26 (5.5438)
167.08553 11.18 28 (0.94789)
182.09643 19.76 10 128 129 131 (1.0721 0.21025 0.20969 0.18296)
216.08078 11.19 70 (0.94849)
218.09643 33.08 69 (2.8045)
220.11208 28.37 68 (2.405)
245.10732 21.35 176 (1.8102)
247.12297 17.75 164 (1.5044)
261.10224 17.32 57 48 54 (1.2609 0.17635 0.031031)
261.13862 50.22 154 (4.2569)
263.11789 18.19 58 49 53 (1.3175 0.13777 0.086747)
265.13354 16.24 210 (1.3767)
289.09715 11.43 59 81 223 (0.75139 0.1345 0.083266)
304.10805 25.61 51 6 82 92 88 86 60 (1.0728 0.54076 0.22506 0.11595 0.11369 0.087165 0.015952)
306.12370 51.68 5 80 67 43 61 73 90 87 83 52 85 89 93 84 221 (1.5198 0.99484 0.57934 0.31905 0.30523 0.26322 0.098867 0.076949 0.058592 0.053349 0.037833 0.028892 0.024712 0.016355 0.0042222)
316.10805 22.92 174 178 175 182 (0.93287 0.78069 0.2031 0.02642)
318.12370 18.26 163 162 (1.3342 0.21338)
320.13935 12.83 165 (1.0879)
330.12370 20.87 151 150 (1.4806 0.28862)
332.10297 17.45 230 232 231 (0.83344 0.35729 0.2883)
332.13935 99.14 153 152 (7.2574 1.1472)
334.11862 49.92 225 219 213 216 218 224 (1.5467 1.1573 0.9335 0.29197 0.21963 0.082415)
336.17065 15.83 157 (1.3422)
338.18630 15.83 158 (1.3422)
349.15467 9.85 259 (0.83541)

0 422.1710472881 Cc1c([NH2+]c2ccc3c(c2)Cc2ccccc2-3)n(C2CC(O)C(CO)O2)c(=O)[nH]c1=O
1 390.1448325401 CC1=C([NH+]=C2C=C3C=C4C=CC=CC4=C3CC2)N(C2C=C(O)CO2)C(O)=NC1=O
2 392.1604826041 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(C2C=C(O)CO2)C(O)=NC1=O
3 404.1604826041 C=C1OC(N2C(O)=NC(=O)C(C)=C2[NH+]=C2C=C3C=C4C=CCCC4=C3CC2)C=C1O
4 308.1393532361 CC1=C(O)N=C(O)N=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1
5 306.1237031721 CC1=C(O)N=C(O)N=C1[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1
6 304.1080531081 CC1=C(O)N=C(O)N=C1[NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1
7 302.0924030441 [CH2+]C1=C(O)N=C(O)N=C1N=C1C=CC2=C3C=CC=CC3=CC2=C1
8 99.0440558841 C=C1OC=CC1[OH2+]
9 101.0597059481 CC1OC=CC1[OH2+]
10 182.0964258041 [NH2+]=C1C=C2C=C3C=CC=CC3=C2CC1
11 221.0556831881 C=C1OC(N2C=C(C)C(=[OH+])N=C2O)=CC1=O
12 223.0713332521 C=C1OC(N2C=C(C)C(=[OH+])N=C2O)=CC1O
13 225.0869833161 C=C1OC(N2C=C(C)C(=[OH+])NC2O)=CC1O
14 386.1499179201 C=C1C=CC(N2C(O)=NC(=O)C(C)=C2[NH+]=C2C=C3C=C4C=CC=CC4=C3CC2)O1
15 404.1604826041 CC(=O)C(O)=C=CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1 Intermediate Fragment 
16 404.1604826041 C#CC(O)=C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
17 404.1604826041 C#COC(=C=CO)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
18 362.1499179201 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(CC#CO)C(O)=NC1=O
19 378.1448325401 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C(=O)C#CO)C(O)=NC1=O
20 404.1604826041 C=C=C([NH+]=C1C=C2C=C3CCCCC3=C2CC1)N(C(=O)N=C=O)C1C=C(O)C(=C)O1 Intermediate Fragment 
21 404.1604826041 C=C1OC(=NC([NH+]=C2C=C3C=C4CCCCC4=C3CC2)=C(C)C(=O)N=C=O)C=C1O Intermediate Fragment 
22 361.1546689521 C=C1OC(=[NH+]C(N=C2C=C3C=C4C=CCCC4=C3CC2)=C(C)C=O)C=C1O
23 404.1604826041 C=C1OC(=NC(O)=NC(=O)C(C)=C=[NH+]C2=CC3=C(CC2)C2=C(C=CCC2)C3)C=C1O Intermediate Fragment 
24 312.1706533641 CC1=C(O)N=C(O)N=C1[NH+]=C1C=C2CC3CCCCC3=C2CC1
25 125.0345538201 C=C1C=NC(O)=NC1=[OH+]
26 127.0502038841 CC1C=NC(O)=NC1=[OH+]
27 129.0658539481 CC1C=NC(O)NC1=[OH+]
28 167.0855267721 C1=CC2=C(C=C1)C1=C(C=[CH2+]C=C1)C2
29 142.0611029161 CC1C(=O)N=C(O)N=C1[NH3+]
30 312.1706533641 CC(C(=N)[NH+]=C1C=C2CC3CCCCC3=C2CC1)=C(O)N=C=O Intermediate Fragment 
31 312.1706533641 CC(=C=O)C(=NC(=N)O)[NH+]=C1C=C2CC3CCCCC3=C2CC1 Intermediate Fragment 
32 312.1706533641 C#CC(=NC(=O)N=C=O)[NH+]=C1CCC2C(C1)CC1CCCCC12 Intermediate Fragment 
33 312.1706533641 C=C=C(O)N=C(O)N=C=[NH+]C1=CC2=C(CC1)C1CCCCC1C2 Intermediate Fragment 
34 312.1706533641 CC(=C=[NH+]C1=CC2=C(CC1)C1=C(CCCC1)C2)C(=O)N=C(N)O Intermediate Fragment 
35 310.1550033001 CC1=C(O)N=C(O)N=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1
36 308.1393532361 CC(C(=N)[NH+]=C1C=C2C=C3C=CCCC3=C2CC1)=C(O)N=C=O Intermediate Fragment 
37 288.1131384881 CC1=CN=C(O)N=C1[NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1
38 288.1131384881 CC1=C(O)N=CN=C1[NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1
39 290.0924030441 OC1=CC([NH+]=C2C=CC3=C4C=CC=CC4=CC3=C2)=NC(O)=N1
40 180.0807757401 [NH2+]=C1C=CC2=C3C=CC=CC3=CC2=C1
41 123.0189037561 [CH+]=C1C=NC(O)=NC1=O
42 140.0454528521 C=C1C(=O)N=C(O)N=C1[NH3+]
43 306.1237031721 CC(C(=N)[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1)=C(O)N=C=O Intermediate Fragment 
44 231.0916747721 C#CC(=[NH2+])N=C1C=CC2=C3C=CC=CC3=CC2=C1
45 233.1073248361 C#CC(=[NH2+])N=C1C=C2C=C3C=CC=CC3=C2CC1
46 235.1229749001 C#CC(=[NH2+])N=C1C=C2C=C3C=CCCC3=C2CC1
47 259.0865893921 [CH2+]C(=C=O)C(=N)N=C1C=CC2=C3C=CC=CC3=CC2=C1
48 261.1022394561 CC(=C=O)C(=[NH2+])N=C1C=CC2=C3C=CC=CC3=CC2=C1
49 263.1178895201 CC(=C=O)C(=[NH2+])N=C1C=C2C=C3C=CC=CC3=C2CC1
50 278.1287885521 C=C(C(=[NH2+])N=C1C=C2C=C3C=CC=CC3=C2CC1)C(N)=O
51 304.1080531081 CC(C(=[NH2+])N=C1C=CC2=C3C=CC=CC3=CC2=C1)=C(O)N=C=O
52 306.1237031721 CC(C(=NC=O)[NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1)=C(N)O Intermediate Fragment 
53 263.1178895201 CC(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C([NH3+])=O
54 261.1022394561 [CH2+]C(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(N)=O
55 74.0600402961 CCC([NH3+])=O
56 235.0865893921 O=C[NH+]=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2
57 261.1022394561 C#CC(N=C1C=C2C=C3C=CC=CC3=C2CC1)=[NH+]C=O
58 263.1178895201 C#CC(N=C1C=C2C=C3C=CCCC3=C2CC1)=[NH+]C=O
59 289.0971540761 CC(=C=O)C(N=C1C=CC2=C3C=CC=CC3=CC2=C1)=[NH+]C=O
60 304.1080531081 CC(C(N=C1C=CC2=C3C=CC=CC3=CC2=C1)=NC#[O+])=C(N)O
61 306.1237031721 CC(=C=O)C(=NC(=N)O)[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1 Intermediate Fragment 
62 246.0913404241 CC(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C#[O+]
63 248.0818383601 N=C(O)N=C=[N+]=C1C=CC2=C3C=CC=CC3=CC2=C1
64 252.1131384881 N=C(O)[NH+]=C=NC1C=C2C=C3C=CC=CC3=C2CC1
65 278.1287885521 C#CC(N=C1C=C2C=C3C=CCCC3=C2CC1)=[NH+]C(=N)O
66 46.0287401681 [NH2+]=CO
67 306.1237031721 C#CC(=NC(=O)N=C=O)[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
68 220.1120758681 C=C=C=[NH+]C1=CC2=C(CC1)C1=C(C=CC=C1)C2
69 218.0964258041 C=C=C=[NH+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2
70 216.0807757401 [CH+]=C=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2
71 259.0865893921 C#CC(N=C1C=CC2=C3C=CC=CC3=CC2=C1)=[NH+]C=O
72 74.0236547881 O=C=[NH+]CO
73 306.1237031721 C=C=C(O)N=C(O)N=C=[NH+]C1=CC2=C(CC1)C1=C(C=CC=C1)C2 Intermediate Fragment 
74 194.0964258041 C=[NH+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2
75 192.0807757401 C#[N+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2
76 237.1022394561 O=C[NH+]=C=NC1=CC2=C(CC1)C1=C(C=CC=C1)C2
77 233.0709393281 [O+]#CN=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2
78 209.1073248361 [NH+]#CN=C1C=C2C=C3C=CCCC3=C2CC1
79 207.0916747721 [NH+]#CN=C1C=C2C=C3C=CC=CC3=C2CC1
80 306.1237031721 CC(=C=[NH+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(=O)N=C(N)O Intermediate Fragment 
81 289.0971540761 CC(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(=O)[NH+]=C=O
82 304.1080531081 [CH2+]C(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(=O)N=C(N)O
83 306.1237031721 [CH2+]C1=C(O)N=C(O)N=C1N=C(C)C=CC1=CCC2=C1C=CC=C2 Intermediate Fragment 
84 306.1237031721 C=CC(C=C1C=C2C=CC=CC2=C1)=[NH+]C1=NC(O)=NC(O)=C1C Intermediate Fragment 
85 306.1237031721 CCC1=C2C=CC=CC2=CC1=C=C=[NH+]C1=NC(O)=NC(O)=C1C Intermediate Fragment 
86 304.1080531081 C=CC1=C2C=CC=CC2=CC1=C=C=[NH+]C1=NC(O)=NC(O)=C1C
87 306.1237031721 [CH2+]C1=C(O)N=C(O)N=C1N=C1C=CC(=C2C=CC=CC2)C(C)=C1 Intermediate Fragment 
88 304.1080531081 [CH+]=C1C(=O)N=C(O)N=C1N=C1C=CC(=C2C=CC=CC2)C(C)=C1
89 306.1237031721 [CH2+]C1=C(O)N=C(O)N=C1N=C1C=C(CC2=CC=CC=C2)C=CC1 Intermediate Fragment 
90 306.1237031721 C=C1C=CC=CC1=C1C=CC(=[NH+]C2=NC(O)=NC(O)=C2C)C=C1 Intermediate Fragment 
91 290.0924030441 [CH+]=C1C(=O)N=C(O)N=C1N=C1C=CC(=C2C=CC=CC2)C=C1
92 304.1080531081 C=C1C=CC=CC1=C1C=CC(=NC2=NC(O)=NC(O)=C2[CH2+])C=C1
93 306.1237031721 C=C=CC1=C2C=CC(=[NH+]C3=NC(O)=NC(O)=C3C)C=C2C=C1C Intermediate Fragment 
94 111.0076703761 O=C1C=COC1=C=[OH+]
95 113.0233204401 OC1C=COC1=C=[OH+]
96 85.0284058201 [OH+]=C1C=COC1
97 113.0233204401 C=C=C(O)C(=O)C=[OH+] Intermediate Fragment 
98 115.0389705041 OC1C=COC1=C[OH2+]
99 87.0440558841 [OH2+]C1C=COC1
100 97.0284058201 [CH+]=C1OCC=C1O
101 97.0284058201 [OH+]=C=C1C=CCO1
102 115.0389705041 C=C=C(O)C(=O)C[OH2+] Intermediate Fragment 
103 97.0284058201 C=C=C(O)C#C[OH2+]
104 57.0334912001 C=C=C[OH2+]
105 115.0389705041 O=C=C=C(O)CC[OH2+] Intermediate Fragment 
106 45.0334912001 C=C[OH2+]
107 43.0178411361 C#C[OH2+]
108 115.0389705041 OC=C=COC=C[OH2+] Intermediate Fragment 
109 117.0546205681 OC1C=COC1C[OH2+]
110 89.0597059481 [OH2+]C1CCOC1
111 99.0440558841 [OH2+]C=C1C=CCO1
112 117.0546205681 C=COC(=C[OH2+])CO Intermediate Fragment 
113 91.0389705041 OCC(=[OH+])CO
114 117.0546205681 C=C=C(O)C(O)C[OH2+] Intermediate Fragment 
115 99.0440558841 C=C=C(O)C=C[OH2+]
116 59.0491412641 CC=C[OH2+]
117 61.0284058201 OCC=[OH+]
118 117.0546205681 OC=C=C(O)CC[OH2+] Intermediate Fragment 
119 89.0597059481 C=C(O)CC[OH2+]
120 117.0546205681 OC=C=COCC[OH2+] Intermediate Fragment 
121 119.0702706321 OC1CCOC1C[OH2+]
122 101.0597059481 [OH2+]CC1C=CCO1
123 119.0702706321 OCC=C(O)CC[OH2+] Intermediate Fragment 
124 101.0597059481 C=C=C(O)CC[OH2+]
125 404.1604826041 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C2C=C(O)C(=C=O)O2)C=NC1=O
126 290.1287885521 CC1=C(O)N=CN=C1[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1
127 184.1120758681 [NH2+]=C1C=C2C=C3C=CCCC3=C2CC1
128 182.0964258041 CCC1=C2C=CC=CC2=CC1=CC#[NH+] Intermediate Fragment 
129 182.0964258041 C=C1C=C2C=CC=CC2=C1CCC#[NH+] Intermediate Fragment 
130 155.0855267721 C=CC1=C2C=CC=CC2=CC1=[CH3+]
131 182.0964258041 C=C1C=CC=CC1=C1C=CC(=[NH2+])C=C1 Intermediate Fragment 
132 235.0349477441 [CH2+]C1=CN(C2C=C(O)C(=C=O)O2)C(O)=NC1=O
133 237.0505978081 CC1=CN(C2C=C(O)C(=C=[OH+])O2)C(O)=NC1=O
134 239.0662478721 CC1=CN(C2C=C(O)C(=C=[OH+])O2)C(O)NC1=O
135 241.0818979361 CC1=CN(C2C=C(O)C(=C=[OH+])O2)C(O)NC1O
136 131.0815040121 CC1C=NC(O)NC1[OH2+]
137 223.0713332521 CC1=CN(C2C=CC(=C=[OH+])O2)C(O)NC1=O
138 241.0818979361 CC1=CN(C=C=C(O)C(=O)C=[OH+])C(O)NC1O Intermediate Fragment 
139 153.0658539481 C#CN1C=C(C)C(=[OH+])NC1O
140 223.0713332521 CC1=CN(C=C=C(O)C#C[OH2+])C(O)NC1=O
141 181.0607685681 CC1=CN(CC#CO)C(O)=NC1=[OH+]
142 241.0818979361 CC1=CN(C(O)=C=C(O)C#C[OH2+])C(O)NC1O Intermediate Fragment 
143 241.0818979361 CC=CN(C(O)NC=O)C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
144 241.0818979361 CC(C=O)=CN(C(N)O)C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
145 241.0818979361 CC(=CN=C1C=C(O)C(=C=[OH+])O1)C(O)NCO Intermediate Fragment 
146 243.0975480001 CC1CN(C2C=C(O)C(=C=[OH+])O2)C(O)NC1O
147 243.0975480001 CC1CN(C(O)=C=C(O)C#C[OH2+])C(O)NC1O Intermediate Fragment 
148 404.1604826041 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C2C=CC(=C=O)O2)C(O)=NC1=O
149 422.1710472881 C=C(OC(=C=O)CO)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
150 330.1237031721 C#CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1
151 330.1237031721 C#CN(C(=O)N=C=O)C(=C=C)[NH+]=C1C=C2C=C3C=CCCC3=C2CC1 Intermediate Fragment 
152 332.1393532361 C#CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1
153 332.1393532361 C#CN(C(=O)N=C=O)C(=C=C)[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
154 261.1386249641 C#C[NH+]=C(C#C)N=C1C=C2C=C3CCCCC3=C2CC1
155 334.1550033001 C#CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1
156 334.1550033001 C#CN(C(=O)N=C=O)C(=C=C)[NH+]=C1C=C2CC3CCCCC3=C2CC1 Intermediate Fragment 
157 336.1706533641 C#CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2CC3CCCCC3=C2CC1
158 338.1863034281 C#CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2CC3CCCCC3C2CC1
159 348.1342678561 C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1
160 350.1499179201 C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1
161 422.1710472881 C=C(O)C(=C=O)OCN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
162 318.1237031721 CC1=C([NH+]=C2C=CC3=C4C=CC=CC4=CC3=C2)N(C)C(O)=NC1=O
163 318.1237031721 C=C=C([NH+]=C1C=C2C=C3C=CC=CC3=C2CC1)N(C)C(=O)N=C=O Intermediate Fragment 
164 247.1229749001 C=C=C(N=C1C=C2C=C3C=CC=CC3=C2CC1)[NH+]=C
165 320.1393532361 CC1=C([NH+]=C2C=C3C=C4C=CC=CC4=C3CC2)N(C)C(O)=NC1=O
166 422.1710472881 CC1=C([NH+]=C2C=C3CC4CCCCC4=C3CC2)N(C=C=C(O)C(=O)C=O)C(O)=NC1=O Intermediate Fragment 
167 87.0076703761 O=CC(=[OH+])C=O
168 89.0233204401 O=CC(=[OH+])CO
169 73.0284058201 OCC#C[OH2+]
170 93.0546205681 OCC([OH2+])CO
171 75.0440558841 OCC=C[OH2+]
172 324.1706533641 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C)C(O)=NC1=O
173 322.1550033001 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(C)C(O)=NC1=O
174 316.1080531081 [CH2+]N1C(O)=NC(=O)C(C)=C1N=C1C=CC2=C3C=CC=CC3=CC2=C1
175 316.1080531081 C=NC([NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1)=C(C)C(=O)N=C=O Intermediate Fragment 
176 245.1073248361 C=C=C(N=C1C=CC2=C3C=CC=CC3=CC2=C1)[NH+]=C
177 273.1022394561 C#[N+]C(=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(C)=C=O
178 316.1080531081 C=C=C([NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1)N(C)C(=O)N=C=O Intermediate Fragment 
179 288.1131384881 C#CC(=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)[N+](=C)C(=N)O
180 273.1022394561 C#CC(=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)[N+](=C)C=O
181 243.0916747721 C#[N+]C(=C=C)N=C1C=CC2=C3C=CC=CC3=CC2=C1
182 316.1080531081 C#CC(=O)N=C(O)N(C)C#[N+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2 Intermediate Fragment 
183 404.1604826041 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C=C=C(O)C#CO)C(O)=NC1=O
184 386.1499179201 C#CC(O)=C=CN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1
185 386.1499179201 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(CC#CC#CO)C(O)=NC1=O
186 360.1342678561 CC1=C([NH+]=C2C=C3C=C4C=CC=CC4=C3CC2)N(CC#CO)C(O)=NC1=O
187 404.1604826041 C=C=C([NH+]=C1C=C2CC3CCCCC3=C2CC1)N(C=C=C(O)C#CO)C(=O)N=C=O Intermediate Fragment 
188 366.1812180481 CC1=C([NH+]=C2C=C3CC4CCCCC4=C3CC2)N(CC#CO)C(O)=NC1=O
189 364.1655679841 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(CC#CO)C(O)=NC1=O
190 346.1550033001 C#CCN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CCCC3=C2CC1
191 364.1655679841 C=C=C([NH+]=C1C=C2CC3CCCCC3=C2CC1)N(CC#CO)C(=O)N=C=O Intermediate Fragment 
192 344.1393532361 C#CCN1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1
193 362.1499179201 C=C=C([NH+]=C1C=C2C=C3CCCCC3=C2CC1)N(CC#CO)C(=O)N=C=O Intermediate Fragment 
194 291.1491896481 C=C=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+]=CC#CO
195 362.1499179201 CC(=C=[NH+]C1=CC2=C(CC1)C1=C(C=CCC1)C2)C(=O)N=C(O)N=CC#CO Intermediate Fragment 
196 360.1342678561 C=C=C([NH+]=C1C=C2C=C3C=CCCC3=C2CC1)N(CC#CO)C(=O)N=C=O Intermediate Fragment 
197 422.1710472881 CC1=C([NH+]=C2C=C3CC4CCCCC4=C3CC2)N(C(O)=C=C(O)C#CO)C(O)=NC1=O Intermediate Fragment 
198 354.1812180481 C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2CC3CCCCC3=C2CC1
199 352.1655679841 C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=C2C=C3CCCCC3=C2CC1
200 346.1186177921 C=C(O)N1C(O)=NC(=O)C(C)=C1[NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1
201 68.9971056921 [O+]#CC#CO
202 71.0127557561 O=CC#C[OH2+]
203 55.0178411361 C#CC=[OH+]
204 77.0597059481 OCCC[OH2+]
205 338.1499179201 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C=O)C(O)=NC1=O
206 336.1342678561 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(C=O)C(O)=NC1=O
207 336.1342678561 CC(C(=O)N=C=O)=C(N=C=O)[NH+]=C1C=C2C=C3CCCCC3=C2CC1 Intermediate Fragment 
208 336.1342678561 CC(C=O)=C([NH+]=C1C=C2C=C3C=CC=CC3=C2CC1)N(C=O)C(=N)O Intermediate Fragment 
209 336.1342678561 C=C=C([NH+]=C1C=C2C=C3CCCCC3=C2CC1)N(C=O)C(=O)N=C=O Intermediate Fragment 
210 265.1335395841 C=C=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+]=C=O
211 336.1342678561 CC(=C=[NH+]C1=CC2=C(CC1)C1=C(C=CCC1)C2)C(=O)N=C(O)N=C=O Intermediate Fragment 
212 336.1342678561 C=C1C=CC=CC1=C1C=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C=O)CC1 Intermediate Fragment 
213 334.1186177921 CC1=C([NH+]=C2C=C3C=C4C=CC=CC4=C3CC2)N(C=O)C(O)=NC1=O
214 153.0294684401 CC1=CN(C#[O+])C(O)=NC1=O
215 155.0451185041 CC1=CN(C=[OH+])C(O)=NC1=O
216 334.1186177921 CC(C(=O)N=C=O)=C(N=C=O)[NH+]=C1C=C2C=C3C=CCCC3=C2CC1 Intermediate Fragment 
217 291.1128041401 CC(C=O)=C(N=C1C=C2C=C3C=CC=CC3=C2CC1)[NH+]=C=O
218 334.1186177921 CC(C=O)=C([NH+]=C1C=CC2=C3C=CC=CC3=CC2=C1)N(C=O)C(=N)O Intermediate Fragment 
219 334.1186177921 C=C=C([NH+]=C1C=C2C=C3C=CCCC3=C2CC1)N(C=O)C(=O)N=C=O Intermediate Fragment 
220 117.0294684401 O=C=[NH+]C(O)NC=O
221 306.1237031721 C=C=C(N=C1C=C2C=C3C=CC=CC3=C2CC1)[NH+](C=O)C(=N)O
222 291.1128041401 C=C=C(N=C1C=C2C=C3C=CC=CC3=C2CC1)[NH+](C=O)C=O
223 289.0971540761 C=C=C(N=C1C=CC2=C3C=CC=CC3=CC2=C1)[NH+](C=O)C=O
224 334.1186177921 C=C=C(O)N=C(O)N(C=O)C=[NH+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2 Intermediate Fragment 
225 334.1186177921 CC(=C=[NH+]C1=CC2=C(CC1)C1=C(C=CC=C1)C2)C(=O)N=C(O)N=C=O Intermediate Fragment 
226 222.1277259321 C=C=C=[NH+]C1=CC2=C(CC1)C1=C(C=CCC1)C2
227 248.1069904881 CC(=C=[NH+]C1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C=O
228 287.0815040121 [CH2+]C(=C=NC1=CC2=C(C=C1)C1=C(C=CC=C1)C2)C(=O)N=C=O
229 291.1128041401 CC(=C=NC1=CC2=C(CC1)C1=C(C=CC=C1)C2)C(=O)[NH+]=C=O
230 332.1029677281 CC1=C([NH+]=C2C=CC3=C4C=CC=CC4=CC3=C2)N(C=O)C(O)=NC1=O
231 332.1029677281 CC(C(=O)N=C=O)=C(N=C=O)[NH+]=C1C=C2C=C3C=CC=CC3=C2CC1 Intermediate Fragment 
232 332.1029677281 C=C=C([NH+]=C1C=C2C=C3C=CC=CC3=C2CC1)N(C=O)C(=O)N=C=O Intermediate Fragment 
233 87.0440558841 C=C(O)C=C[OH2+]
234 71.0491412641 C=CC(=C)[OH2+]
235 71.0491412641 C#CCC[OH2+]
236 91.0753560121 CC(O)CC[OH2+]
237 73.0647913281 C=C([OH2+])CC
238 380.1604826041 CC1=C([NH+]=C2C=C3CC4CCCCC4=C3CC2)N(C(=O)C#CO)C(O)=NC1=O
239 378.1448325401 C=C=C([NH+]=C1C=C2CC3CCCCC3=C2CC1)N(C(=O)C#CO)C(=O)N=C=O Intermediate Fragment 
240 376.1291824761 CC1=C([NH+]=C2C=C3C=C4C=CCCC4=C3CC2)N(C(=O)C#CO)C(O)=NC1=O
241 47.0491412641 CC[OH2+]
242 422.1710472881 CC1=C([NH+]=C2C=C3CC4CCCCC4=C3CC2)N(C(=C=CO)OC#CO)C(O)=NC1=O Intermediate Fragment 
243 422.1710472881 C=C=C([NH+]=C1C=C2CC3CCCCC3C2CC1)N(C(=O)N=C=O)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
244 394.1761326681 C=C=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+](C(=N)O)C1C=C(O)C(=C=O)O1
245 351.1703190161 C=C=C(N=C1C=C2CC3CCCCC3C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
246 379.1652336361 C=C=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+](C=O)C1C=C(O)C(=C=O)O1
247 377.1495835721 C=C=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+](C=O)C1C=C(O)C(=C=O)O1
248 375.1339335081 C=C=C(N=C1C=C2C=C3C=CCCC3=C2CC1)[NH+](C=O)C1C=C(O)C(=C=O)O1
249 333.1597543321 C=C=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C)O1
250 226.1590260601 CC#C[NH+]=C1C=C2CC3CCCCC3=C2CC1
251 333.1597543321 C=C=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+]=C1C=CC(=C=O)O1
252 351.1703190161 C#CC(N=C1C=C2CC3CCCCC3=C2CC1)=[NH+]C(=C)OC(=C=O)CO Intermediate Fragment 
253 351.1703190161 C=C=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+]=COC(=C=O)C(=C)O Intermediate Fragment 
254 351.1703190161 C=C=C(N=C1C=C2CC3CCCCC3C2CC1)[NH+]=C=C=C(O)C(=O)C=O Intermediate Fragment 
255 351.1703190161 C#CC(N=C1C=C2CC3CCCCC3C2CC1)=[NH+]C(O)=C=C(O)C#CO Intermediate Fragment 
256 309.1597543321 C#CC(N=C1C=C2CC3CCCCC3C2CC1)=[NH+]C(=O)C#CO
257 307.1441042681 C#CC(N=C1C=C2CC3CCCCC3=C2CC1)=[NH+]C(=O)C#CO
258 305.1284542041 C#CC(N=C1C=C2C=C3CCCCC3=C2CC1)=[NH+]C(=O)C#CO
259 349.1546689521 C=C=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
260 347.1390188881 C=C=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
261 422.1710472881 CC(C=O)=C([NH+]=C1C=C2C=C3CCCCC3=C2CC1)N(C(=N)O)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
262 405.1444981921 CC(C=O)=C(N=C1C=C2C=C3C=CCCC3=C2CC1)[NH+](C=O)C1C=C(O)C(=C=O)O1
263 379.1652336361 CC(C=O)=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
264 379.1652336361 CC(=C=O)C(N=C1C=C2CC3CCCCC3=C2CC1)=[NH+]C(O)=C=C(O)C#CO Intermediate Fragment 
265 377.1495835721 CC(C=O)=C(N=C1C=C2C=C3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
266 422.1710472881 CC(C(N)=O)=C([NH+]=C1C=C2C=C3CCCCC3=C2CC1)N(C=O)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
267 422.1710472881 CC(C(=O)N=C=O)=C(N=C1C=C(O)C(=C=O)O1)[NH+]=C1C=C2CC3CCCCC3C2CC1 Intermediate Fragment 
268 394.1761326681 CC(C(N)=O)=C(N=C1C=C2CC3CCCCC3=C2CC1)[NH+]=C1C=C(O)C(=C=O)O1
269 422.1710472881 CC(=C=[NH+]C1=CC2=C(CC1)C1CCCCC1C2)C(=O)N=C(O)N=C1C=C(O)C(=C=O)O1 Intermediate Fragment 
270 250.1226405521 CC(=C=[NH+]C1=CC2=C(CC1)C1=C(C=CC=C1)C2)C=O
271 160.0604342201 OC=C1OC([NH2+]CO)C=C1O
272 158.0447841561 O=C=C1OC([NH2+]CO)C=C1O
273 130.0498695361 [NH3+]C1C=C(O)C(=CO)O1
274 128.0342194721 [NH3+]C1C=C(O)C(=C=O)O1
275 293.1284542041 CC(=C=NC1=CC2=C(CC1)C1=C(C=CCC1)C2)C(=O)[NH+]=C=O
276 295.1441042681 CC(=C=NC1=CC2=C(CC1)C1=C(CCCC1)C2)C(=O)[NH+]=C=O
277 422.1710472881 C=C=C(O)N=C(O)N(C=[NH+]C1=CC2=C(CC1)C1=C(CCCC1)C2)C1C=C(O)C(=C=O)O1 Intermediate Fragment 
278 325.1546689521 O=C=C1OC(=[NH+]C=NC2=CC3=C(CC2)C2CCCCC2C3)C=C1O
279 325.1546689521 OC#CC(O)=C=C(O)[NH+]=C=NC1=CC2=C(CC1)C1CCCCC1C2 Intermediate Fragment 
280 422.1710472881 CC(CCC1=CCC2=C1CCC=C2)=[NH+]C1=C(C)C(=O)N=C(O)N1C1C=C(O)C(=C=O)O1 Intermediate Fragment 
281 422.1710472881 CCC(C=C1C=C2CCCCC2=C1)=[NH+]C1=C(C)C(=O)N=C(O)N1C1C=C(O)C(=C=O)O1 Intermediate Fragment 
282 422.1710472881 CCC1=C2CCCCC2CC1=C=C=[NH+]C1=C(C)C(=O)N=C(O)N1C1C=C(O)C(=C=O)O1 Intermediate Fragment 
283 422.1710472881 C=C1C=C2CCCCC2=C1CCC=[NH+]C1=C(C)C(=O)N=C(O)N1C1C=C(O)C(=C=O)O1 Intermediate Fragment 
284 422.1710472881 CC1=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C2C=C(O)C(=C=O)O2)CCC1=C1C=CCCC1 Intermediate Fragment 
285 406.1397471601 CC1=C([NH+]=C2C=CC(=C3C=CC=CC3)CC2)N(C2C=C(O)C(=C=O)O2)C(O)=NC1=O
286 422.1710472881 CC1=C([NH+]=C2C=C(CC3=CCCC=C3)CCC2)N(C2C=C(O)C(=C=O)O2)C(O)=NC1=O Intermediate Fragment 
287 344.1240970961 CC1=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C2C=C(O)C(=C=O)O2)CCC1
288 342.1084470321 CC1=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C2C=C(O)C(=C=O)O2)CC=C1
289 230.0924030441 C=C1C(=O)N=C(O)N=C1[NH+]=C1C=C(C)C=CC1
290 342.1084470321 CC1=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C=C=C(O)C(=O)C=O)CC=C1 Intermediate Fragment 
291 340.0927969681 [CH2+]C1=C(N=C2C=C(C)C=CC2)N(C2C=C(O)C(=C=O)O2)C(O)=NC1=O
292 330.1084470321 CC1=C([NH+]=C2C=CCCC2)N(C2C=C(O)C(=C=O)O2)C(O)=NC1=O
293 422.1710472881 C=C1CCCCC1=C1C=CC(=[NH+]C2=C(C)C(=O)N=C(O)N2C2C=C(O)C(=C=O)O2)CC1 Intermediate Fragment 
294 422.1710472881 CCCC1=C2CCC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)C=C2C=C1C Intermediate Fragment 
295 422.1710472881 CCC1=CC2=CC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)CCC2=C1CC Intermediate Fragment 
296 396.1553972241 CCC1=C2CCC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)C=C2CC1
297 422.1710472881 CCCC1=CC2=CC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)CCC2=C1C Intermediate Fragment 
298 394.1397471601 CC1=CC2=CC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)CCC2=C1C
299 422.1710472881 CCCCC1=CC2=CC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)CCC2=C1 Intermediate Fragment 
300 422.1710472881 CCCCC1=C2CCC(=[NH+]C3=C(C)C(=O)N=C(O)N3C3C=C(O)C(=C=O)O3)C=C2C=C1 Intermediate Fragment 
301 420.1553972241 CC1=C([NH+]=C2C=C3C=C4CCCCC4=C3CC2)N(C2C=C(O)C(=C=O)O2)C(O)=NC1=O
