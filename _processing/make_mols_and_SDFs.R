# THIS DOES NOT WORK
# OPENBABEL CANNOT CONVERT CDXML CORRECTLY


# Libraries ---------------------------------------------------------------
library(readxl)
library(ChemmineOB)
library(dplyr)
library(fs)
library(purrr)
library(withr)
library(magrittr)
library(readxl)
library(conflicted)

dir.create("mol")
dir.create("tempmol")


files <- list.files("Chemdraw", "\\.cdxml$", full.names = TRUE)



# Make mol files ----------------------------------------------------------
for( i in seq_along(files)){

  infile <- paste0(getwd(),"/",files[i])
  outfile <-   basename(files[i]) %>% 
                  path_sanitize %>% 
                  gsub(' ', '-', .) %>% 
                  paste0(dirname(files[i]), "/", .) %>% 
                  paste0(getwd(),"/",.) %>% 
                  gsub("\\.cdxml$", ".mol" ,.) %>% 
                  gsub("/Chemdraw/", "/tempmol/" ,.) %>% 
                  normalizePath(mustWork = FALSE)
  
  
  logfile <- paste0(getwd(),"/tempmol/log.log") %>% normalizePath(mustWork = FALSE)
  

  
  stdout_file <- tempfile(fileext = ".log")
  stderr_file <- tempfile(fileext = ".log")
  
  system2(command="obabel", 
                  args=paste0('-icdxml "',infile,'" -omol -O "',outfile, '" --gen3D -xh -h'), 
                  stdout=stdout_file, 
                  stderr=stderr_file, 
                  wait=TRUE)
  
  
write("\n================================================================",file=logfile,append=TRUE)
write(basename(infile),file=logfile,append=TRUE)
write(readLines(stdout_file),file=logfile,append=TRUE)
write(readLines(stderr_file),file=logfile,append=TRUE)

file.remove(stdout_file)
file.remove(stderr_file)
  
}





# Find conversion problems ------------------------------------------------
readLines("tempmol/log.log") %>% 
  paste(sep = "", collapse = "\n") %>% 
  strsplit("\n================================================================", fixed = TRUE) %>% 
  map(~grep(".cdxml\n\n1 molecule converted", .,  value =TRUE, fixed = TRUE, invert = TRUE)) %>% 
  extract2(1) %>% 
  gsub("^\\n", "\n================================================================\n", .) %>% 
  writeLines("tempmol/log_problems.log")
  





# Re-convert mol-files after manual fixes ---------------------------------










