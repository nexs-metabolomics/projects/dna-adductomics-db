#In-silico ESI-MS/MS [M+H]+ Spectra
#PREDICTED BY CFM-ID 4.0.8
#InChI=1S/C12H13N5O3/c18-4-8-7(19)3-9(20-8)17-6-14-10-11-13-1-2-16(11)5-15-12(10)17/h1-2,5-9,18-19H,3-4H2/t7-,8-,9-/m1/s1
#InChiKey=XQQIMTUYVDUWKJ-IWSPIJDZSA-N
energy0
160.06177 100.00 2 35 38 37 30 27 16 (62.566 0.053927 0.037242 0.025938 0.016338 0.00060429 0.00055905)
276.10912 49.68 0 110 85 63 59 131 122 116 119 124 115 50 120 139 57 58 56 123 (30.634 0.20735 0.13745 0.078554 0.034242 0.011041 0.010904 0.0088258 0.0073185 0.0071361 0.0038876 0.0031414 0.001546 0.0011368 0.00045768 0.00019506 8.1165e-05 7.9925e-06)
energy1
160.06177 100.00 2 37 35 30 38 27 16 (95.577 0.45843 0.44112 0.35654 0.3535 0.20748 0.038734)
energy2
45.03349 4.42 44 (2.5208)
57.03349 7.22 49 (4.1179)
94.03997 2.43 19 (1.3866)
133.05087 12.21 20 31 (4.5732 2.3912)
145.05087 2.43 26 24 (1.0082 0.37505)
160.06177 100.00 2 35 37 38 30 16 27 (27.422 13.189 7.0396 4.0225 2.5379 1.6726 1.1513)
162.07742 8.90 8 18 15 21 23 22 25 17 (2.591 1.0515 0.46422 0.23741 0.22452 0.20476 0.1605 0.1439)
166.10872 3.36 12 13 (1.6743 0.24157)

0 276.1091157281 OC1CC(n2cnc3c2ncn2ccnc32)OC1C[OH2+]
1 248.1142011081 [OH2+]C1C=C(N2C=NC3C4NC=CN4C=NC32)OC1
2 160.0617716121 C1=CN2C=NC3=NC=NC3=C2[NH2+]1
3 246.0985510441 [OH2+]C1=COC(N2C=NC3C4NC=CN4C=NC32)=C1
4 228.0879863601 C1=COC(N2C=NC3=C4[NH2+]C=CN4C=NC32)=C1
5 244.0829009801 [OH2+]C1=COC(N2C=NC3=C4NC=CN4C=NC32)=C1
6 258.0985510441 C=C1OC(N2C=NC3=C4NC=CN4C=NC32)C=C1[OH2+]
7 240.0879863601 [CH+]=C1C=CC(N2C=NC3=C4NC=CN4C=NC32)O1
8 162.0774216761 C1=CN2C=NC3NC=NC3=C2[NH2+]1
9 258.0985510441 C#CC([OH2+])=C=C(O)N1C=NC2C3NC=CN3C=NC21 Intermediate Fragment 
10 258.0985510441 [OH+]=C=C1C=CC(N2C=NC3C4NC=CN4C=NC32)O1
11 164.0930717401 C1=CN2C=NC3NC=NC3C2[NH2+]1
12 166.1087218041 C1=CN2C=NC3NCNC3C2[NH2+]1
13 166.1087218041 C=[NH+]C1C2N=CNC2N=CN1C Intermediate Fragment 
14 164.0930717401 C=NC1NC=NC1=C1NC=C[NH2+]1 Intermediate Fragment 
15 162.0774216761 C=[NH+]C1=C2N=CN=C2N=CN1C Intermediate Fragment 
16 160.0617716121 C#[N+]C1=C2N=CN=C2N=CN1C
17 162.0774216761 [NH2+]=C=CN1C=NC2NC=NC2=C1 Intermediate Fragment 
18 162.0774216761 C=NC1=NC=NC1=C1NC=C[NH2+]1 Intermediate Fragment 
19 94.0399735481 C#[N+]C1=CN=CN1
20 133.0508725801 C1=NC=NC1=C1N=CC=[NH+]1
21 162.0774216761 C1=CN(C=NC2=CN=CN2)C=[NH+]1 Intermediate Fragment 
22 162.0774216761 N=CN1C=C[NH2+]C1=C1C=NC=N1 Intermediate Fragment 
23 162.0774216761 N=C=NC1C=C2[NH2+]C=CN2C=N1 Intermediate Fragment 
24 145.0508725801 C#[N+]C1=CC2=NC=CN2C=N1
25 162.0774216761 C=NC1=C2[NH2+]C=CN2C=NC1=N Intermediate Fragment 
26 145.0508725801 C#[N+]C1=CN=CN2C=CN=C12
27 160.0617716121 C=C=[NH+]C1=C2N=CN=C2N=CN1 Intermediate Fragment 
28 134.0461215481 [NH2+]=C1N=CN=C2N=CN=C12
29 158.0461215481 [C+]#CNC1=C2N=CN=C2N=CN1
30 160.0617716121 [NH2+]=C=CN1C=NC2=NC=NC2=C1 Intermediate Fragment 
31 133.0508725801 [CH2+]N1C=NC2=NC=NC2=C1
32 143.0352225161 [C+]#CN1C=NC2=NC=NC2=C1
33 40.0181754841 [C+]#CN
34 158.0461215481 N=C=[C+]N1C=NC2=NC=NC2=C1
35 160.0617716121 C=NC1=NC=NC1=C1N=CC=[NH+]1 Intermediate Fragment 
36 158.0461215481 C#[N+]C1=NC=NC1=C1N=CC=N1
37 160.0617716121 N=C=NC1=CC2=[NH+]C=CN2C=N1 Intermediate Fragment 
38 160.0617716121 N=C=NC1=CN=CN2C=C[NH+]=C12 Intermediate Fragment 
39 115.0389705041 OC1C=COC1=C[OH2+]
40 117.0546205681 OC1C=COC1C[OH2+]
41 99.0440558841 C=C1OCC=C1[OH2+]
42 99.0440558841 [OH2+]C=C1C=CCO1
43 117.0546205681 C=COC(=C[OH2+])CO Intermediate Fragment 
44 45.0334912001 C=C[OH2+]
45 117.0546205681 OC=C=COCC[OH2+] Intermediate Fragment 
46 117.0546205681 OC=C=C(O)CC[OH2+] Intermediate Fragment 
47 99.0440558841 C=C=C(O)C=C[OH2+]
48 117.0546205681 C=C=C(O)C(O)C[OH2+] Intermediate Fragment 
49 57.0334912001 CC#C[OH2+]
50 276.1091157281 C=NC1C2N=CN(C3C=C(O)C(=C=[OH+])O3)C2N=CN1C Intermediate Fragment 
51 245.0669165681 [CH2+]N1C=NC2C(=C1)N=CN2C1C=C(O)C(=C=O)O1
52 247.0825666321 CN1C=NC2C(=C1)N=CN2C1C=C(O)C(=C=[OH+])O1
53 249.0982166961 CN1C=NC2C(C1)N=CN2C1C=C(O)C(=C=[OH+])O1
54 249.0982166961 CN1C=NC2C(C1)N=CN2C(O)=C=C(O)C#C[OH2+] Intermediate Fragment 
55 274.0934656641 C=[NH+]C1=C2N=CN(C3C=C(O)C(=C=O)O3)C2N=CN1C
56 276.1091157281 C=C=NC1NC=NC2C1NCN2C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
57 276.1091157281 N=C=CN1C=NC2C(C1)NCN2C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
58 276.1091157281 C=CN1C=NC2C(N=CN2C2C=C(O)C(=C=[OH+])O2)C1N Intermediate Fragment 
59 276.1091157281 C=C(OC(=C=[OH+])CO)N1C=NC2C3NC=CN3C=NC21 Intermediate Fragment 
60 204.0879863601 C=C([OH2+])N1C=NC2=C1N=CN1C=CNC21
61 202.0723362961 C=C([OH2+])N1C=NC2=C1N=CN1C=CN=C21
62 186.0774216761 [CH2+]#CN1C=NC2=C3NC=CN3C=NC21
63 276.1091157281 OC=C=C(OC#C[OH2+])N1CNC2C3NC=CN3C=NC21 Intermediate Fragment 
64 246.0985510441 C=C(OC#C[OH2+])N1C=NC2C3NC=CN3C=NC21
65 248.1142011081 C=C(OC#C[OH2+])N1CNC2C3NC=CN3C=NC21
66 234.0985510441 [OH2+]C#COCN1C=NC2C3NC=CN3C=NC21
67 232.0829009801 [OH2+]C#COCN1C=NC2=C3NC=CN3C=NC21
68 230.0672509161 OC#C[O+]=CN1C=NC2=C3NC=CN3C=NC21
69 234.0985510441 OC(C#C[OH2+])N1C=NC2C3NC=CN3C=NC21
70 216.0879863601 C#CC(=[OH+])N1C=NC2C3NC=CN3C=NC21
71 216.0879863601 [OH2+]C#CCN1C=NC2=C3NC=CN3C=NC21
72 232.0829009801 O=C(C#C[OH2+])N1C=NC2C3NC=CN3C=NC21
73 230.0672509161 O=C(C#C[OH2+])N1C=NC2=C3NC=CN3C=NC21
74 47.0491412641 CC[OH2+]
75 29.0385765801 C=[CH3+]
76 214.0723362961 OC#C[CH+]N1C=NC2=C3NC=CN3C=NC21
77 172.0617716121 [CH2+]N1C=NC2=C1N=CN1C=CN=C21
78 214.0723362961 C=NC1C(=C2N=CC=N2)N=CN1[CH+]C#CO Intermediate Fragment 
79 188.0930717401 [CH2+]#CN1C=NC2C1N=CN1C=CNC21
80 198.0774216761 [C+]#CCN1C=NC2=C3NC=CN3C=NC21
81 216.0879863601 C=NC1C(=C2NC=CN2)N=CN1[CH+]C#CO Intermediate Fragment 
82 218.1036364241 [OH2+]C#CCN1C=NC2C3NC=CN3C=NC21
83 200.0930717401 C#[CH+]CN1C=NC2=C3NC=CN3C=NC21
84 220.1192864881 [OH2+]C#CCN1CNC2C3NC=CN3C=NC21
85 276.1091157281 OC(=C=C(O)N1CNC2C3NC=CN3C=NC21)C#C[OH2+] Intermediate Fragment 
86 75.0440558841 OCC=C[OH2+]
87 73.0284058201 OCC#C[OH2+]
88 43.0178411361 C#C[OH2+]
89 55.0178411361 C#CC=[OH+]
90 71.0127557561 O=CC#C[OH2+]
91 200.0566862321 [CH+]=C(O)N1C=NC2=C1N=CN1C=CN=C21
92 206.1036364241 C=C([OH2+])N1C=NC2C3NC=CN3C=NC21
93 208.1192864881 C=C([OH2+])N1CNC2C3NC=CN3C=NC21
94 192.0879863601 [OH+]=CN1C=NC2C1N=CN1C=CNC21
95 190.0723362961 [OH+]=CN1C=NC2=C1N=CN1C=CNC21
96 188.0566862321 [OH+]=CN1C=NC2=C1N=CN1C=CN=C21
97 186.0410361681 [O+]#CN1C=NC2=C1N=CN1C=CN=C21
98 87.0440558841 C=C(O)C=C[OH2+]
99 69.0334912001 C#CC(=C)[OH2+]
100 69.0334912001 C#CC=C[OH2+]
101 89.0597059481 C=C(O)CC[OH2+]
102 71.0491412641 C=CC(=C)[OH2+]
103 71.0491412641 C#CCC[OH2+]
104 91.0753560121 CC(O)CC[OH2+]
105 73.0647913281 C=C([OH2+])CC
106 258.0985510441 OC(=C=CN1C=NC2C1N=CN1C=CNC21)C#C[OH2+]
107 240.0879863601 C#CC([OH2+])=C=CN1C=NC2=C3NC=CN3C=NC21
108 240.0879863601 [OH2+]C#CC#CCN1C=NC2=C3NC=CN3C=NC21
109 274.0934656641 OC(=C=C(O)N1C=NC2C3NC=CN3C=NC21)C#C[OH2+]
110 276.1091157281 OC(=C=[OH+])C(O)=C=CN1CNC2C3NC=CN3C=NC21 Intermediate Fragment 
111 184.0617716121 [C+]#CN1C=NC2=C3NC=CN3C=NC21
112 184.0617716121 [C+]#CN1C=NC(=C2N=CC=N2)C1N=C Intermediate Fragment 
113 188.0930717401 [CH2+]#CN(C=N)C1C=C2NC=CN2C=N1 Intermediate Fragment 
114 174.0774216761 [CH4+]N1C=NC2=C1N=CN1C=CN=C21
115 276.1091157281 C=C(O)C(=C=[OH+])OCN1C=NC2C3NC=CN3C=NC21 Intermediate Fragment 
116 276.1091157281 C=NC1C(C2NC=CN2)N=CN1C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
117 206.0560175361 C=[NH+]C1=CN=CN1C1C=C(O)C(=C=O)O1
118 208.0716676001 C=[NH+]C1CN=CN1C1C=C(O)C(=C=O)O1
119 276.1091157281 OC1=CC(N2C=NCC2N=CN2C=CNC2)OC1=C=[OH+] Intermediate Fragment 
120 276.1091157281 N=CN1C=CNC1C1CN(C2C=C(O)C(=C=[OH+])O2)C=N1 Intermediate Fragment 
121 249.0982166961 OC1=CC(N2CNC(C3N=CC=N3)C2)OC1=C=[OH+]
122 276.1091157281 CN1C=CNC1=C1N=CN(C2C=C(O)C(=C=[OH+])O2)C1N Intermediate Fragment 
123 276.1091157281 CN(C1C=C(O)C(=C=[OH+])O1)C1N=CN2C=CNC2=C1N Intermediate Fragment 
124 276.1091157281 N=CN(C1CC2NC=CN2C=N1)C1C=C(O)C(=C=[OH+])O1 Intermediate Fragment 
125 259.0825666321 C=[N+](C1C=C2NC=CN2C=N1)C1C=C(O)C(=C=O)O1
126 259.0825666321 C=[N+](C=C=C(O)C(O)=C=O)C1C=C2NC=CN2C=N1 Intermediate Fragment 
127 249.0982166961 O=C=C1OC([NH2+]C2CC3NC=CN3C=N2)C=C1O
128 155.0451185041 [NH2+]=CNC1C=C(O)C(=C=O)O1
129 120.0556236121 C1=CC2=[NH+]C=CN2C=N1
130 120.0556236121 C=C=[NH+]C1=NC=NC=C1 Intermediate Fragment 
131 276.1091157281 OC1=CC(NC=NC2CN=CN3C=CNC23)OC1=C=[OH+] Intermediate Fragment 
132 142.0498695361 C[NH2+]C1C=C(O)C(=C=O)O1
133 114.0549549161 C=[NH+]C1=CC(O)CO1
134 147.0665226441 C=[NH+]C1=CN=CN2C=CN=C12
135 147.0665226441 C#[N+]C=CN=CN1C=CN=C1 Intermediate Fragment 
136 151.0978227721 C=[NH+]C1CN=CN2C=CNC12
137 124.0869237401 C1=CN2C=NCCC2[NH2+]1
138 122.0712736761 C1=CC2[NH2+]C=CN2C=N1
139 276.1091157281 C=NC1C(NC2C=C(O)C(=C=[OH+])O2)N=CN2C=CNC12 Intermediate Fragment 
140 247.0825666321 O=C=C1OC(=[NH+]C2CC3NC=CN3C=N2)C=C1O
